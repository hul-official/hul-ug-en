# Practical Usage

In this chapter, a few tips are introduced, which are good to know when using HUL.

## Generation and download of MCS with Vivado

**Generation of MCS**<br>
How to generate MCS using Vivado is explained. An alternative tool, iMpact, is omitted, because few people use it. <br>
Open the project in Vivado and select tool → Generate Memory Configuration File. Fill information as in
[Figure](#GENMCS) and press OK to generate MCS. This screen shot is taken from Vivado 2020.1.

As of 2021, there are three kinds of SPI flash memory used in HUL. Each should be configured in Vivado as follows.
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">IC on board</span></th>
    <th class="nowrap"><span class="mgr-20">Manufacturer</span></th>
    <th class="nowrap"><span class="mgr-20">Part ID on Vivado</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>N25Q128A</td>
    <td>Micron</td>
    <td>mt25ql128-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>MT25QL512</td>
    <td>Micron</td>
    <td>mt25ql512-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>S25FL256S</td>
    <td>Spansion</td>
    <td>s25fl256sxxxxxx0-spi-x1_x2_x4</td>
  </tr>
  </tbody>
</table>

![gen-mcs](genmcs.png "Generation of MCS"){: #GENMCS width=90% }

**How to download MCS by Hardware manager**<br>
Connect the JTAG cable with the HUL powered on. The LED of the download cable will turn green, if correctly connected. If it stays orange, the USB connection may be taken by the client OS, if virtual machine is used on PC. With the green LED on, access the FPGA with "Open Hardware manager" → "Open target" in Vivado. Kintex7 (XC7K160T-1) should be discovered. If you want to write a Bit stream file, right-click the FPGA, assign the Bit stream file, and write it. MCS file should be downloaded to the SPI flash memory, however, the memory will not appear on the screen at this point. This is because the SPI flash memory does not exist yet on the JTAG chain.

To make SPI visible, add the configuration memory device. Right-click on the FPGA and select "Add configuration memory device". A screen like [Figure](#DLMCS) will appear, select the appropriate device in the Memory Device (MCS has to be created accordingly). Select both MCS and PRM files, and leave everything else as default. Then OK to write; it takes about 10 minutes to write.

![dlmcs](DLMCS.png "Configuration Memory Device"){: #DLMCS width=60% }

**For N25Q128A**<br>
Micron's N25Q128A is the chip selected when designing the HUL, but it has been discontinued. The latest version of Vivado does not support this chip anymore. However, N25 series is internally compatible with the MT25 series, which is the successor chip. When generating the MCS for N25, select **mt25ql128** and make it as MT25QL. Also, when downloading use the setting for MT25QL.

**For MT25QL512**<br>
In late 2017, the mass-purchased board changed the SPI flash memory to MT25QL. When generating MCS file, [Figure](#GENMCS) select **mt25ql512_spi-x1_x2_x4** for this SPI memory. In Hardware manager, select the same for memory devise in [Figure](#DLMCS).

**For S25FL256SAGNFI001**<br>
Since 2018, SPI flash memory chip is changed to Spansion S25FL256SAGNFI0. Select **s25fl256sxxxxxxx1-spi-x1_x2_x4** when making MCS and downloading it by Hardware manager.

**Failure of FPGA initialization when JTAG is connected, Hardware manager is up and HUL is powered on**<br>
You will encounter this problem if you turn on the power with the download cable connected while the Hardware manager is running and the JTAG server is open. Even if the Hardware manager is running, it can be avoided if the server is closed.

### MCS download to Mezzanine HR-TDC

As of 2021, the mezzanine HR-TDC has two types of flash memory, as described in Chapter 2. Please select the following part number when generating MCS and downloading.
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">IC on the board</span></th>
    <th class="nowrap"><span class="mgr-20">Manufacturer</span></th>
    <th class="nowrap"><span class="mgr-20">Part ID on Vivado</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>N25Q128A</td>
    <td>Micron</td>
    <td>mt25qu128-spi-x1_x2_x4</td>
  </tr>
  <tr>
    <td>MT25QU256A</td>
    <td>Micron</td>
    <td>mt25qu256-spi-x1_x2_x4</td>
  </tr>
  </tbody>
</table>

## MCS download via SiCPT by FMP

MCS may be downloaded to SPI flash memory via the network (RBCP) by using the Flash Memory Programmer (FMP). Without this technology, it was necessary to insert the USB download cable into the JTAG socket on the board, so someone must go and reach to the board. Replacing the FW during the experiment was a big task. By using FMP, you can download MCS from the measurement room remotely, and reconfigure the FPGA without touching the hardware.

Prepare the same MCS file as writing with JTAG. The executable to write to SPI flash memory is in `hul_software.git/Common/bin`. Execute as follows, with a -b option if a binary file has been generated by
`mcs_converter`.
```
./bin/flash_memory_progorammer [ip-address] [mcs file path]
```
If the download is successful and the verify result matches the original MCS, the following is displayed. If communication is interrupted or a mismatch happens, please retry again until being successful. At this stage, the firmeware is only downloaded to the SPI flash memory, and not reflected in the FPGA. After successfully downloaded, use `reconfig_fpga` to reconfigure the FPGA.

![fmp](fmp-download.png "Console display after successful MCS download with FMP."){: #FMP width=90% }

**Known issues**<br>
It takes a certain amount of time to write a page in the flash memory, and it may not be completed before UDP RBCP turns one cycle, depending on the machine specifications of the PC and the communication environment. The current FMP is not designed to block the next write operation while writing a page, and if the write request for the next page is issued too early, communication may be interrupted or a UDP RBCP bus error may occur. Currently, `usleep` is forcibly delaying the next write, but it is not a good solution and it takes a long time to write. In the future, the issue may be handled on the FPGA side, but not yet tackled.

## A few How-to's in usage

**A simple test**<br>
Here is a simple test method for HUL RM, HUL Scaler, and HUL MH-TDC. These firmwares will work without inserting the module into the VME crate. In `daq_func.cc`, select `TRM::L1Ext` to be the only input in `TRM::SelectTrig`. Connect the trigger signal to NIMIN1. To monitor the data, execute `./bin/daq`.

**Having multiple HUL modules in one crate**<br>
With multiple HULs inserted in the crate, only one can be the J0 bus master, and others to be in the slave mode.  If there are two masters, J0 bus will be short-circuited. (If KEK VME crate is not used, there will be no issue.)

**How to set Master mode**<br>
Mount HRM on Slot-U.

* Turn on all DIP SW1. (If this is ON, it will be in driver mode for J0 bus.)
* Turn on the 2nd bit (mezzanine HRM) and 4th bit (Bus BUSY) of DIP SW2. (The firmware must support HRM.)
* Set `TRM::EnRM` to be 1 and EnJ0 to be 0.

**How to set Slave mode**<br>

* Turn off all DIP SW1.
* `TRM::EnJ0` to be 1 and EnRM to be 0.
Try force busy (DIP SW2 bit 3 ) to be ON on a Slave module, and check HRM busy LED to be ON (correctly configured).

**A problem after module reset, when receiving Level2 trigger in KEK VME J0 bus.**<br>
There was a problem of DAQ not run after a module reset (e.g. BCT reset) was issued. The version 3 firmware has solved the problem. Please report if there is a problem.

**Event slips**<br>
It is possible to monitor event slips via J0 bus and HRM Tag.
If the event numbers do not agree between the modules, an event slip has occured.
Since HUL firmware has a multi-event buffer, RUN Start/Stop will not solve the slip. Clear the internal buffers by `BCT::Reset`.

**How to use HR-TDC**<br>
HR-TDC will hang up easily in mis-operations, because the system has two FPGAs and there is a initialization procedure. For the first-time use, the procedure written here is recommended. **Be sure to use a VME crate.**

1. Insert the Mezzanine HR-TDC into the HUL and download the MCS to each FPGA. It is recommended to start with MCS as the BCT may hang if you use the bit stream after turning on the power. On Mezzanine HR-TDC, specify  mt25qu128-spi-x1_x2_x4, when downloading MCS.
2. Turn the crate power on / off once.
3. Prepare the software. If two Mezzanine HR-TDCs are installed, you can use the distributed C++ code as it is. If only one is installed, set `EnSlotUp / EnSlotDown` in RegisterMap.hh to be false for the empty slot.
4. Run `./bin/debug`. The version of the HUL and mezzanine firmwares will be displayed. If a bus error is issued here, there is a configure problem.  First check the software, and if there is no mistake, try detach / install the mezzanine card(s). The high speed communications through the mezzanine slot tend to cause the bus error, and require a good contact.
5. Execute `./bin/initialize` to initialize DDR and generate estimator using calibration clock. Make sure both are successful.
6. The distributed C++ code assumes NIMIN1 should have a Common stop. Connect the trigger to NIMIN 1 and execute `./bin/daq [run no] [# of events]`. This should return the data in the subdirectory, such as ./data/run[run no].dat.
7. To create a ROOT file from the binary data, execute `./bin/decode [run no]`. It will create a ROOT file under `./rootfile/run[run no].root. See the software section for more detail about the TTree structure.
## Miscellaneous

**How to purchase modules from GND Ltd**<br>
Send an email to GND Co., Ltd., telling the **GND catalog number** and **its number of purchase**. If in stock, the modules will be  delivered immediately. HUL controller module includes the SiTCP license  fee in the price.
If the module is out of stock, we must consult the manufacture plan. Independent orderwill result to a high price, because the initial cost for production will be split to the number of modules manufactured.






