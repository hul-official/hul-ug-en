# Firmware

## Version.1 and  Version. 3 series

Major update of the firmware was done in August 2021 from version 1 to version 3. There is no software compatibility between them, because of the change of the local bus controller (BCT) structure.
Version.2 is an internal development and not released.
Version.1 uses some of the `RBCP address` as the data bus, but version.3 uses the full `RBCP address`  (32bit) and the full `RBCP data` (8 bit) as they are.
The modification above seamlessly enables `FPGAModule` class as a wrapper to `RBCP` class.
In the DAQ / slowcontrol software for version 3 firmware, `RBCP address` specifies the local modules, making  `ModuleID` and `LocalAddress` in version 1 obsolete.
`ModuleID` and `LocalAddress` reflects the Bus Controller (BCT) structure of version 1, which information is irrelevant to the (non developper) users.

Version.3 is newly equipped with two local modules common to all: Flash Memory Programmer (FMP) and Self Diagnosis System (SDS). FMP writes SPI flash memory via SiTCP. It has enabled a remote downloading of the firmware (MCS file) to EEPROM via ethernet, eliminating the offline download procedure using a USB-JTAG cable. SDS diagnoses the status of FPGA and the board. It audits the number of soft error corrections caused  by radiations, or detects an un-recoverable error. Surveillance of FPGA temperatures and critical voltages (VCCINT, VCCAUX and VCCBRAM) are also implemented.

## Basic structure of HUL firmware
There are two types of HUL firmware: with or without data acquisition by the PC. An example for the former is TDC, and the latter is a trigger logic. The latter has the individual block structure depending on the function, but  the former (data-acquisition) has a common block structure. The following is the common block diagram.
The components shown in <span class="myred">orange</span> in the figure are the functions implemented in all firmware.
The `local bus` that controls each local module is controlled by the local bus controller (BCT), which is connected to SiTCP's RBCP (UDP).
FMP and SDS are local modules implemented in all version.3 firmware, and their functions are as described above.

The <span class="myred">white squares</span> in the figure represent the parts related to data acquisition. They are called a measurement block, such as TDC and scaler. These features are mainly implemented in the block labeled as *User circuit*.
The data from these *user circuits* is collected in the event builder block via the builder bus, attached with event headers, and then transferred to the PC via SiTCP.
The Receiver block, trigger manager, and J0 interface are the blocks involved in trigger I / O.
HUL is designed to receive trigger and event tag information sent by Master Triger Module (MTM: GNN-570).
The receiver block is activated when the HRM mezzanine card is installed, and gains the role of receiving the trigger from the upstream circuit.
This block is also connected to the builder bus to transfer the received trigger information to the PC when the trigger is received.
When using the receiver block, HUL is most likely the J0 bus master in the VME crate.
The J0 interface sends the trigger and tag information received by the receiver block to the J0 bus if the HUL is the J0 bus master.
In the case of HUL being a J0 slave, the trigger information coming from the J0 bus is passed to the trigger manager.
To make HUL a general-purpose module, it supports trigger input from the NIM port in the absence of MTM.
The trigger manager is responsible for trigger distribution inside the firmware, and  the trigger input source may be selected from the HRM mezzanine (receiver block), NIM input, and J0 bus input.
The *LEVEL2*, *Fast clear*, and *TAG* information sent by the Trigger manager is used internally by the event builder to generate event header information and determine the data transmission to PC.

![daqtype-block](fw-overview.png "Block diagram of data-acquisition-type firmware "){: width=90% }

## Commons to all HUL firmware

*hul_software* package manages the addresses of BCT, FMP, and SDS, the common parts of the firmware, in the file `common/src/RegisterMapCommon.hh`.

### Reset sequence

From the update of December 19, 2017, the reset sequence of all firmware has been standardized.
In the previous firmware, there was no way of resetting other than turning off the power, when the BCT hangs.
In MH-TDC, some FIFOs were not reset even if BCT reset was applied, and the DAQ hang state was not cleared.
Therefore, a few reset levels have been introduced to deal with the problem.

**Reset procedures (from milder to harder)**

- BCT reset: Writing BCT Reset command in the designated address with RBCP. User modules under BCT control are reset in the firmware signal. Normally used.
- SiTCP reset: Calling `Reset_SiTCP` defined in ` sitcp_controller.cc`. Will be reset even if the BCT is hung or deadlocked.
- Hard reset: Pressing SW3 on the board. All circuits including SiTCP are reset. Most enforceable reset procedure of all.

Use BCT reset normally, and if BCT deadlocks by a mis-operation, use SiTCP reset. If SiTCP hangs as well, use Hard reset.

### Network speed supported

The currently released firmware, SiTCP only works in the 1 GbE mode.
SiTCP supports both 100 Mbps and 1 GbE modes and can be configured to automatically switch. However, the existing firmware disables the automatic switching function for the  convenience of the PHY chip being used.
100 Mbps mode will never be implemented, because there would be no needs for it anymore. Do not connect to a 100Mbps-only network switch. SiTCP will not communicate.

### Bus controller (BCT)

Bus controller (BCT) has the functions of issuing a BCT reset, acquiring a version numer, and reconfiguring the FPGA, in addition to the usual functions to access the local modules.
The special functions are available by executing the following operations to the listed RBCP address.

**RBCP address for special functions of BCT  (Module ID: 0xE)**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Register </span></th>
    <th class="nowrap"><span class="mgr-10">address</span></th>
    <th class="nowrap"><span class="mgr-10">operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
        <th>memo</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Reset</td>
    <td class="tcenter">0xE000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Asserting module reset signal to all modules except SiTCP.</td>
  </tr>
  <tr>
    <td>Version</td>
    <td class="tcenter">0xE010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Returns Firmware ID and version number. Needs to read 4 bytes.</td>
  </tr>
  <tr>
    <td>Reconfig</td>
    <td class="tcenter">0xE020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Sends Low to PROG_B_ON to re-configure FPGA. SiTCP connection will be immediately closed, and may be reconnected in a few seconds.</td>
  </tr>
</tbody>
</table>

### Flash Memory Programmer (FMP)

FMP sends SPI commands from the PC and execute supported functions by the memory chip, such as read / write page data.
Such SPI commands include  erase, write and read memory. The same operations which Vivado does to the memory (erase, write, and verify) has now become available over the network by FMP.
<span class="myred">There is a known bug in FMP</span>: the next page write request is accepted  before the precious command finishes. Currently, the software simply waits a sufficient amount of time before sending the next page write command, but this is not a very good solution.
Eventually, the FMP module may be corrected.
Because of this wait time, it takes longer time to write than the network speed anticipates.
If write-failures occur frequently, please contact the author of this document.
The RBCP address is listed below;  it is not recommended to control  FMP from anything other than  `FlashMemoryProgrammer.cc` included in the *hul_software* package.
If the write-lock bit is accidentally set, that memory block may never be writitten again.

**RBCP address of FMP (Module ID: 0xD)**

<table class="vmgr-table">
  <thead><tr>
      <th class="nowrap"><span class="mgr-10">Register </span></th>
    <th class="nowrap"><span class="mgr-10">address</span></th>
    <th class="nowrap"><span class="mgr-10">operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
        <th>memo</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Status</td>
    <td class="tcenter">0xD000'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>Obtain the status bit of FMP module.<br>
    Currently, the lowest bit is assigned to SPI command cycle busy.
    </td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0xD000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Mode change of SPI sequence. <br>
      bit 1-2: SPI sequence mode
      <ul>
        <li>0x0: Read mode</li>
        <li>0x1: Write mode</li>
        <li>0x2: Instruction mode</li>
      </ul>
      bit 3: Dummy mode <br>
      Sets chip select to be OFF, making the flash memory immune to the SPI sequence.
    </td>
  </tr>
  <tr>
    <td>Register</td>
    <td class="tcenter">0xD020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SPI command</td>
  </tr>
  <tr>
    <td>InstLength</td>
    <td class="tcenter">0xD030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>SPI command length</td>
  </tr>
  <tr>
    <td>ReadLength</td>
    <td class="tcenter">0xD040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">10</td>
    <td>page read length</td>
  </tr>
  <tr>
    <td>WriteLength</td>
    <td class="tcenter">0xD050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">10</td>
    <td>page write length </td>
  </tr>
  <tr>
    <td>ReadCountFIFO</td>
    <td class="tcenter">0xD060'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">10</td>
    <td>read count for FIFO where the page-read data are stored.</td>
  </tr>
  <tr>
    <td>ReadFIFO</td>
    <td class="tcenter">0xD070'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>8-bit (byte) wide readout from the read FIFO.</td>
  </tr>
  <tr>
    <td>WriteCountFIFO</td>
    <td class="tcenter">0xD080'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">10</td>
    <td>write count for FIFO where the page-write data are stored.</td>
  </tr>
  <tr>
    <td>WriteFIFO</td>
    <td class="tcenter">0xD090'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">8</td>
    <td>8-bit (byte) wide writein to the write FIFO.</td>
  </tr>
  <tr>
    <td>Execute</td>
    <td class="tcenter">0xD100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>execute the SPI sequence.</td>
  </tr>
</tbody>
</table>


### Self Diagnosis System (SDS)

SDS is a self-diagnosis program.
Soft Error Mitigation (SEM) and XADC, IP cores of Xilinx FPGA, are implemented and monitored.
SEM is an IP core that detects, corrects, and classifies single event upsets (SEUs).
SDS detects the number of errors corrected and the occurrence of un-correctable errors.
If the system falls into an uncorrectable state, reconfiguring the FPGA from the flash memory or performing a power cycle is necessary.
Also, it is possible to intentionally inject SEU, but it will be an advanced use of SDS;  please check how to use SEM in the Xilinx User Guide.

XADC is the collections of built-in ADCs in Xilinx FPGA
HUL obtains FPGA temperature, VCCINT, VCCAUX, VCCBRAM voltages via XADC.

<span class="myred">SEM has an unresolved issue.</span> The incorrectable error staus may become 1 after the power is turned on in some FPGAs.
The cause is not clear, but in such cases, reset the SEM once by executing `reset_sem` in the *hul_software* package after turning on the power.

**RBCP address of SDS (Module ID: 0xC)**

<table class="vmgr-table">
  <thead><tr>
   <th class="nowrap"><span class="mgr-10">Register </span></th>
    <th class="nowrap"><span class="mgr-10">address</span></th>
    <th class="nowrap"><span class="mgr-10">operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
        <th>memo</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>SdsStatus</td>
    <td class="tcenter">0xC000'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>Obtain the status bit of SDS module.</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>XadcDrpMode</td>
    <td class="tcenter">0xC010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>Select DRP mode of XADC.<br>
        <ul>
          <li> 0x0: Read mode</li>
          <li> 0x1: Write mode</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>XadcDrpAddr</td>
    <td class="tcenter">0xC020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>Supply DRP address of XADC.</td>
  </tr>
  <tr>
    <td>XadcDrpDin</td>
    <td class="tcenter">0xC030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">16</td>
    <td>Supply DRP data for input to XADC.</td>
  </tr>
  <tr>
    <td>XadcDrpDout</td>
    <td class="tcenter">0xC040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>Obtain DRP data  from XADC.</td>
  </tr>
  <tr>
    <td>XadcExecute</td>
    <td class="tcenter">0xC0F0'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Execute DRP access to XADC.</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>SemCorCount</td>
    <td class="tcenter">0xC100'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>Number of corrections to SEU by SEM.</td>
  </tr>
  <tr>
    <td>SemRstCorCount</td>
    <td class="tcenter">0xC200'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Reset SemCorCount.</td>
  </tr>
  <tr>
    <td>SemErroAddr</td>
    <td class="tcenter">0xC300'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">40</td>
    <td>Supply address to `inject_address` port of SEM.</td>
  </tr>
  <tr>
    <td>SemErroStrobe</td>
    <td class="tcenter">0xC400'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Send a pulse to `inject_strobe` port of SEM.</td>
  </tr>
</tbody>
</table>

**Contents of SdsStatus**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">bit number </span></th>
    <th class="nowrap"><span class="mgr-10">Status</span></th>
    <th>memo</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">Over temp</td>
    <td>Indicates that the FPGA temperature has exceeded 125 °C. Turn off the power of HUL immediately to cool down, because there is a serious lack of cooling.</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Temp alarm</td>
    <td>Indicates that the FPGA temperature has exceeded 85 °C. The cooling capacity is most likely insufficient.</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">VCCINT alarm</td>
    <td>It indicates that the voltage of VCCINT has exceeded the normal range (0.97-1.03V). Some trouble is occurring on the board.</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Reserved</td>
    <td> </td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Watchdog alarm</td>
    <td>Indicates that the SEM heartbeat signal is absent. Some trouble is occurring in the SEM.</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Uncorrectable error</td>
    <td>Indicates that the SEM has detected an uncorrectable radiation error. FPGA needs to be reconfigured.</td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Reserved</td>
    <td> </td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Reserved</td>
    <td> </td>
  </tr>
  </tbody>
</table>

## HUL Skeleton

This project is the minimum configuration of firmware that implements SiTCP but does almost nothing. Please use it as a sample when making firmware for HUL. There are only two functions, one is to take OR of the input signals and output it to NIM, and the other is to illuminate the LED with SiTCP or DIP. For the input signals, the fixed inputs (main input ports) and mezzanine inputs (assuming DCR v1 or v2) are grouped in 32 channels of one connector and their OR signal appears in the four NIM OUTs.
Skeleton is misspelled in the VHDL source, but left as it is, because a correction influences in a wide range of codes.

An example for using the NIM-Ex mezzanine card is included under `sources_1 / example_design /`.
Please refer to the toplevel.vhd enclosed.

**Firmware ID and the current version**

When Version is read from BCT, a 32-bit register is returned. Of these, the upper 16 bits are the Firmware ID, and the lower 16 bits are the version numbers (major version 8bit + minor version 8bit).
The current HUL Skeleton ID and versions are as follows:
<table class="vmgr-table">
  <tbody>
  <tr>
    <td>Firmware ID</td>
    <td class="tcenter">0x0000</td>
  </tr>
  <tr>
    <td>Major version</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x02</td>
  </tr>
  </tbody>
</table>

In the following, versions are written in a format like v3.2.

**Version history**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Version</span></th>
    <th class="nowrap"><span class="mgr-10">Release date</span></th>
    <th>Modifications</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">-</td>
    <td class="tcenter">-</td>
    <td>There is no versions up to v2.x</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2021.08.01</td>
    <td>Updated to Version.3</td>
  </tr>
  </tbody>
</table>

### Register map

**RBCP address of LED (Module ID: 0x0)**

**LED assignment**<br>
LED1-3 reflects LED register or bit 2-4 of SW2.
LED4 is connected to "Over temp" flag of SDS.

## HUL RM

By mounting the Mezzanine HRM, the HUL RM can be a J0 bus master and can operate as a DAQ module that reads the data received by the HRM. It is the basis of the MH-TDC and scaler firmware, and should be used as a starting point when developing a new DAQ type firmware.


Since the response to the trigger input is based on this firmware, here we will explain in detail the trigger system and the response of the event builder to it.

**Firmware ID and the current version**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>Firmware ID</td>
    <td class="tcenter">0x0415</td>
  </tr>
  <tr>
    <td>Major version</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x02</td>
  </tr>
  </tbody>
</table>


**Version history**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Version</span></th>
    <th class="nowrap"><span class="mgr-10">Release date</span></th>
    <th>Modifications</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.0</td>
    <td class="tcenter">2016.12.23</td>
    <td>Initial release</td>
  </tr>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2017.01.15</td>
    <td>RVM data header changed from 0x9C to 0xF9.</td>
  </tr>
  <tr>
    <td class="tcenter">v1.2</td>
    <td class="tcenter">2017.01.27</td>
    <td>Vivado update 2016.2 => 2016.4 TRM middle buffer changed from disperse RAM to BRAM. The depth changed from 128 to 256. Prog Full threshold introduced. The reason for the depth of 256 is to be below SCR block depth. RVM middle buffer depth is also changed to 256. Functionality seen from the outside of the module stays the same.  </td>
  </tr>
  <tr>
    <td class="tcenter">v1.3</td>
    <td class="tcenter">2017.03.22</td>
    <td>Solved the problem that the initial register of IOM was not set correctly. Solved the problem that the number of words written in header 2 became 0 in the first event after turning on the power.</td>
  </tr>
  <tr>
    <td class="tcenter">v1.4</td>
    <td class="tcenter">2017.05.09</td>
    <td>Solved the problem of not responding to Clear (BUSY stays standing).</td>
  </tr>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter"> </td>
    <td>Solved the problem that HRM hangs when Clear is entered. (Replaced by v1.6 without release.)</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">2017.08.22</td>
    <td>Fixed an issue where DAQ would hang if a hard reset was entered within ~2 us after the trigger. A new register for each block to select its response to the hard reset. Added new local address.</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2017.12.19</td>
    <td>Standardlized reset sequence. Bit 24 of Header3 is now  indicating whether HRM exists (to be exact, whether DIP2 is ON).</td>
  </tr>
  <tr>
    <td class="tcenter">v1.8</td>
    <td class="tcenter">2018.02.02</td>
    <td>
Solved the bug that the event tag coming from the J0 bus was latched too early and the event number on the HRM side deviated by 1.</td>
  </tr>
  <tr>
    <td class="tcenter">v2.x</td>
    <td class="tcenter"> - </td>
    <td>un released</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2021.08.01</td>
    <td>Added FMP and SDS. Installed Builder bus. Changed the structure of Local bus.</td>
  </tr>
  </tbody>
</table>

**Overview of module function**<br>
HUL RM data acquisition block diagram is shown [below](#RMFW). The modules which do not relate to data acquisision are omitted.
The function of HUL RM is the processing of information received and decoded by Mezzanine HRM. Therefore, the function is <b> implemented only in mezzanine slot U</b>. The information received by Mezzanine HRM is distributed in three routes. The first is the Receiver Module (RVM), which stores lock bit, spill number increment, spill number (full 8bit), and event number (full 12bit) information and passes them to the EVent Builder (EVB). Data is passed to the Event builder via the builder bus, so the RVM information is contained in the data body. That is, the RVM is part of the measurement block for the EVB. The second route is the distribution of trigger information to the J0 bus. At this stage, the event number is reduced to 3-bit and the spill number is reduced to 1-bit. The third is the input to TRigger Manager (TRM).

The HUL firmware has a module called trigger manager (TRM) that manages the internal trigger distribution. TRM receives trigger signals from three ports, trig Ext (NIM IN), J0 bus (if slave), and HRM (if any), and the register sets which port to receive the trigger.

TRM distributes level1 trigger, level2 trigger, clear, and event number (3-bit) and spill number (1-bit) information to each measurement block and EVB. For EVB, these are not DAQ data, so the tags from TRM are embedded within the event hearder. The event number and spill number distributed by TRM have been reduced to 3-bit and 1-bit, because this information must be independent to whether HUL is a master or a slave to the J0 bus. If Tag is not received, both event number and spill number will be 0.

I/O Manager (IOM) is a module that controls which FPGA internal signal is assigned to the NIM input / output ports.

There three types of busy signals. First is the module busy, whici is a logical sum of internal blocks in firmware. Second is the J0 bus busy received from the J0 bus; this is a logical sum of busy signals from HUL, which are slave to the J0 bus. Last is the crate busy; this is a logical sum of the module busy, the J0 bus busy, and the external busy received from NIM-IN. We use the J0 busy and the crate busy, when HUL is a master to the J0 bus.

![rm-fw](rm-fw.png "HUL RM firmware block diagram"){: #RMFW width=90% }

### Register map of HUL RM

The following is a map dedicated to HUL RM. Even if a module or register with the same name exists in other firmware, it does not necessarily have the same address. Be sure to set according to this map (or RegisterMap.hh and namespace of the distributed software).

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Register</span></th>
    <th class="nowrap"><span class="mgr-10">Address</span></th>
    <th class="nowrap"><span class="mgr-10">Operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
    <th>description</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>Selects trigger port in TRM</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>ON/OFF of DAQ gate. TRM disables trigger out if zero. </td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Write to this address asserts a soft reset to EVB, and self event counter in Event builder becomes zero. (Don't care about the register value.)</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT1.</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT2.</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT3.</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT4.</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL1.</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL2.</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext clear.</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext busy.</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter">0x2080'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext rsv2.</td>
  </tr>
</table>

**Trigger Manager (TRM)**<br>
TRM decides which input port signal to be used as a trigger, and sends L1, L2, and Clear signals to the FPGA. Also, Tag signal is repeated, if received, for a redistribution in FPGA. Which port signal is selected is set by the 12-bit register `SelectTrigger`. The relationship between the trigger signal path and `SelectTrigger` is summarized in [Figure](#TRM). Which port receives the L1 trigger is determined by the 3 bits. Please note that if two or more bits are set, the trigger will come out in OR. Once the L1 route is determined, it will be ANDed with the DAQ gate (DAQ controller management) and distributed as the L1 trigger. The selection of L2 trigger and Clear is also performed with the 3 bits, but these are affected by EnL2 bit after routing. If EnL2 is 0, L2 contains a copy of L1 and Clear is always 0. In a simple system without MTM-RM, set EnL2 to 0. L2 is distributed as L2 trigger after ANDed with the DAQ gate.
Tag information source is selected between J0 or HRM using EnJ0 and EnRM bits, respectively. If both are set to 1, Tag will not be issued.

![trm-block](trm-block.png "Trigger route in TRM"){: #TRM width=90% }

A list of register values to be stored in  `TRM::SelectTrigger` address. Since each bit is a switch for the appropriate selector, this register is a 12-bit wide bit string instead of an integer value. Exceptionally, only `RegEnJ0` is used outside of TRM. Only when `RegEnJ0` is high and DIP SW2 No. 2 (mezzanine HRM) is low, module busy is sent to J0 bus. If you want to insert a module into the crate but do not want to affect the J0 bus, set this register `RegEnJ0` to 0.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register label</span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>RegL1Ext</td>
    <td>0x001</td>
    <td>NIMIN provides L1 trigger.</td>
  </tr>
  <tr>
    <td>RegL1J0</td>
    <td>0x002</td>
    <td>J0 bus provides L1 trigger.</td>
  </tr>
  <tr>
    <td>RegL1RM</td>
    <td>0x004</td>
    <td>Mezzanine HRM provides L1 trigger.</td>
  </tr>
  <tr>
    <td>RegL2Ext</td>
    <td>0x008</td>
    <td>NIMIN provides L2 trigger.</td>
  </tr>
  <tr>
    <td>RegL2J0</td>
    <td>0x010</td>
    <td>J0 bus provides L2 trigger.</td>
  </tr>
  <tr>
    <td>RegL2RM</td>
    <td>0x020</td>
    <td>Mezzanine HRM provides L2 trigger.</td>
  </tr>
  <tr>
    <td>RegClrExt</td>
    <td>0x040</td>
    <td>NIMIN provides Clear.</td>
  </tr>
  <tr>
    <td>RegClrJ0</td>
    <td>0x080</td>
    <td>J0 bus provides Clear.</td>
  </tr>
  <tr>
    <td>RegClrRM</td>
    <td>0x100</td>
    <td>Mezzanine HRM provides Clear.</td>
  </tr>
  <tr>
    <td>RegEnL2</td>
    <td>0x200</td>
    <td>0: L2=L1 trigger、1: L2=L2 input</td>
  </tr>
  <tr>
    <td>RegEnJ0</td>
    <td>0x400</td>
    <td>Tag information from J0 bus. If this bit is 1, module busy is sent to J0 bus. </td>
  </tr>
  <tr>
    <td>RegEnRM</td>
    <td>0x800</td>
    <td>Tag information from HRM.</td>
  </tr>
</tbody>
</table>

**I/O Manager (IOM)**<br>
IOM has the function of assigning the signal inside the FPGA to NIMIN or NIMOUT. For example, if Reg_o_ModuleBusy is set to AddrNnimout1, the BUSY signal will be output to NIM output 1 on the front panel. If Reg_i_Nimin1 is set for AddrExtL1, NIM input No. 1 is assigned to the L1Ext line of TRM. The register values that may be stored in the register address are summarized below. The syntax of assignments are opposite between NIMOUTs and NIMINs; NIMOUTs has the address where the signal register value is written in, whereas for inputs, signal has the address where NIMIN values is written in. The register values are interpreted as integers, meaning exclusive with each other.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register label </span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>Signals available for NIMOUT</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busy, meaning only the busy status of the module. J0 bus busy or ExtBusy are not included.</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusy, including the module busy, J0 bus busy and ExtBusy. Usage of this signal assumes that HUL is the bus master, and HRM returns the same busy to master trigger module (MTM).</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRM L1 trigger as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>L2 trigger as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>Clear as HRM has received.</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>Rserve 1 signal as HRM has received.</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>Spill Number Increment of HRM</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gate in DCT</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>ch 8 of DIP SW2</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHz clock </td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHz clock</td>
  </tr>
   <tr><td class="tcenter" colspan=3><b>NIMIN ports available</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>If this register value is set, the default assignment are done for signal lines (see next table), including the NIMOUTs.</td>
  </tr>
</tbody>
</table>

IOM default assignments as listed below.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM output ports</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Signal</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10">default</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### DIP SW and LED on HUL RM

**DIP SW2 functions**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Switch number</span></th>
    <th class="nowrap"><span class="mgr-30">function</span></th>
    <th class="nowrap"><span class="mgr-10">detail</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ON for SiTCP forced default (192.168.10.16). Must be set before power on. </td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Mezzanine HRM</td>
    <td>ON for Mezzanine HRM installed. The actual effect by this mode is described later. This bit status appears in data header3.</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Force BUSY</td>
    <td>Forced high for Crate Busy and Module Busy. Used for connection check. </td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Bus BUSY</td>
    <td>ON to include J0 bus busy to Crate Busy. OFF otherwise. </td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">LED</td>
    <td>ON to turn on LED4.</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Level</td>
    <td>Appears as IOM DIP8 signal. </td>
  </tr>
</tbody>
</table>


**The effect of Mezzanine HRM (DIP SW2 #2)**<br>
If this bit is ON, a few functions change with regard to Mezzanine HRM.

*Event Builder includes RVM in the event packet.*<br>
If this bit is ON, the information in RVM is read by Event Builder and includes into the event packet.

*Mezzanine base (U) signal direction change*<br>
If this bit is ON, a few signal lines to be LVDS output as required by Mezzanine HRM. <br>
if this bit is OFF,  all the slot lines will become LVDS inputs.

*J0 bus master mode to be ON*<br>
If this bit is ON, L1, L2, Clear, Tag information are sent to the J0 bus, and the BUSY signal is received from J0 bus. To be a J0 bus master, it is also necessary to turn all DIP SW1 ON.

*J0 bus slave mode to be OFF*<br>
If this bit is OFF, L1, L2, Clear, Tag will not be accepted from J0 bus.

Below is the relation between J0 bus signals and the trigger signals.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">J0 bus</span></th>
    <th class="nowrap"><span class="mgr-20">Trigger signal</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">S1</td>
    <td>RM_Clear</td>
  </tr>
  <tr>
    <td class="tcenter">S2</td>
    <td>RM_Level2</td>
  </tr>
  <tr>
    <td class="tcenter">S3</td>
    <td>RM_SpillNumber(0)</td>
  </tr>
  <tr>
    <td class="tcenter">S4</td>
    <td>RM_Level1</td>
  </tr>
  <tr>
    <td class="tcenter">S5</td>
    <td>RM_EventNumber(0)</td>
  </tr>
  <tr>
    <td class="tcenter">S6</td>
    <td>RM_EventNumber(1)</td>
  </tr>
  <tr>
    <td class="tcenter">S7</td>
    <td>RM_EventNumber(2)</td>
  </tr>
</tbody>
</table>

**LED indication**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED number</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>Light when TCP connection is open.</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>Light when module busy is high.</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>Light when DIP SW2 #2 (Mezzanine HRM) is ON.</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>Light when DIP SW2 #5 (LED) is ON.</td>
  </tr>
</tbody>
</table>

### DAQ behavior of HUL RM

This section describes data flow and DAQ behavior. The DAQ function consists of each measurement module (hereinafter referred to as the measurement block) and the Event Builder module. The data flow is shown in [Figure](#DAQRM). When the trigger is received, each measurement block processes the data according to the determined operation and saves it in the block buffer. The measurement block has an internal block buffer that can temporarily store multi-events. Event Builder reads the data from each measurement block and continues to build events unless the event buffer is full. Therefore, the DAQ function operates synchronously with the trigger until the data is written to the block buffer;  the subsequent processing does not depend on the external signals nor states and continues to build and transfer the data as long as the data link speed allows.
For HUL RM, the measurement blocks are only RVM and TRM. Strictly speaking, TRM is not a measurement block because the TRM information is not included in the data body but in the header. The only event-built information is RVM data, and the TRM information is used to control data transfer. The TRM stores whether the L2 trigger or Clear was sent in the Nth event, and this information is used by the Event Builder to decide whether to send this event packet to SiTCP or just drop it. Therefore, HUL's DAQ function does not have a fast clear nor clear BUSY functions. All events raised by the L1 trigger are digitized and built once. However, the self-event number assigned by Event Builder is not incremented unless it is forwarded.

RVM latches the information shown in [Figure](#DAQRM) at the timing of L2 trigger and saves it in the block buffer.

![daq-rm-fw](daq-rm-fw.png "DAQ data flow of HUL RM firmware"){: #DAQRM width=90% }

**The timing when Module Busy is asserted**<br>
The definition of Module BUSY in HUL RM is the OR of the BUSY signals listed below. Block full and SiTCP full occurs only when network forwarding can not keep up, so usually BUSY is a fixed length of 160 ns. Currently, Self-busy, set to 160 ns, is rather long, and it may be shortened in the future.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">BUSY type</span></th>
    <th class="nowrap"><span class="mgr-30">BUSY length</span></th>
    <th class="nowrap"><span class="mgr-10">memo</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">160 ns</td>
    <td>Asserted with a fixed length since the L1 trigger is detected.</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>BUSY is output when the block buffer is full. It is asserted when the L1 trigger rate exceeds the data processing speed of the subsequent circuit. In other words, it means that TCP transfer cannot catch up, so it is practically equivalent to SiTCP full.</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>Asserted when the TCP buffer of SiTCP is Full, meaning that the amount of data which the Event Builder is trying to send is too large for the network bandwidth.</td>
  </tr>
</tbody>
</table>

**Data structure**<br>
In HUL, 32-bit is one word, and the three-word header and variable-length data body are one event block.
<br>**Header word**

```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF0415                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "00000"    |         Number of word (11-bit)         |
```
"Number of word" indicates the number of words contained in the data body, not including the header.
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
If *HRM* exist is 1, ch2 of DIP SW2 is on, meaning HRM was installed. This means that data body has a RVM word.
*Tag* " is the 4-bit Tag information from TRM. The lower 3 bits are the lower 3 bits of the RM Event Number,
and the 4th bit is the least significant bit of the RM spill number. "Self counter" is a local event number that is
incremented each time an event is forwarded. Starts with 0.

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```

*Lock* means the RM lock bit, must be 1. *SNI* means Spill Number Increment, which becomes 1 at the change of the Spill Number (not tested if it is true). *Spill Num* is the spill number, and *Event Num* is the event number, received by HRM, respectively.
<!-- ![data-rm-fw](data-rm-fw.png "Data structure of HUL RM firmware"){: #DATARM width=90% } -->

## HUL Scaler

HUL Scaler is a firmware that adds a scaler function to  HUL RM. The implemented scaler is a 28-bit synchronization counter that samples at 300 MHz. Since the HUL Scaler has many functions in common with the HUL RM, only the differences will be mentioned.
**Firmware ID and current version**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>ID</td>
    <td class="tcenter">0x4ca1</td>
  </tr>
  <tr>
    <td>Major version</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x03</td>
  </tr>
  </tbody>
</table>


**Version history**

Exactly same with HRM, except the current version is v3.3.

**Overview of Module functions**<br>
HUL Scaler is implemented for Mezzanine slot D and on-board input ports, in addition to Mezzanine slot U configured for HRM.  DCR v1 (v2) is assumed to  be installed in the Mezzanine slot(s), implementing scalers up to 128 channels. It is also possible to mount HRM (instead of DCR) on Mezzanine slot U to become a J0 bus master like HUL RM firmware. In this case, the Ch 0-31 assigned to slot U is deleted from the data.

The Scaler consists of a 300 MHz, 28-bit long counter that latches the counter at the timing of the L1 trigger and writes it to the buffer. The HUL scaler has two new internal signals connected to the IOM: one is the *spill gate*, which enables the scalers only while this signal is high. The other is *counter reset*, which resets all counts to zero ​​when it becomes high. Enable/Disable  NIM input counter reset is set with *enable_hdrst* (in v1.6 and later). Other features are common to HUL RM.

![scaler-fw](scaler-fw.png "Structure of HUL Scaler firmware"){: #SCALERRM width=90% }

### Register map for HUL Scaler

The following is a map dedicated to HUL Scaler. The difference in the signal and address from HUL RM is marked as <span class="myred">red</span>.
Signal names and the address are defined in RegisterMap.hh and namespace in the software package.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Register</span></th>
    <th class="nowrap"><span class="mgr-10">Address</span></th>
    <th class="nowrap"><span class="mgr-10">Operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
    <th>Description</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>Selects trigger port in TRM</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>ON/OFF of DAQ gate. TRM disables trigger out if zero.</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Write to this address asserts a soft reset to EVB, and  self event counter in Event builder becomes zero. (Don't care about the register value.)</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: SCR (module ID = 0x02)</b></td></tr>
   <tr>
    <td><span class="myred">CounterReset</span></td>
    <td class="tcenter,"><span class="myred">0x2000'0000</span></td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Asserts Software counter reset</td>
  </tr>
  <tr>
    <td><span class = "myred">EnableBlock</span></td>
    <td class="tcenter"><span class = "myred">0x2010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Enable/Disable the input blocks. High for enable. The bit corresponds to: <br>
      bit1: On-board U <br>
      bit2: On-board D <br>
      bit3: Mezzanine U <br>
      bit4: Mezzanine D </td>
  </tr>
  <tr>
    <td><span class = "myred">EnableHdrst</span></td>
    <td class="tcenter"><span class = "myred">0x2020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Enable/Disable the NIM input hardware counter reset for input blocks. High for enable. The bit corresponds to:<br>
         bit1: On-board U <br>
         bit2: On-board D <br>
         bit3: Mezzanine U <br>
         bit4: Mezzanine D </td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter"><span class = "myred">0x3000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT1.</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter"><span class = "myred">0x3010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT2.</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter"><span class = "myred">0x3020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT3.</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter"><span class = "myred">0x3030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT4.</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter"><span class = "myred">0x3040'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL1.</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter"><span class = "myred">0x3050'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL2.</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter"><span class = "myred">0x3060'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext clear.</td>
  </tr>
  <tr>
    <td><span class = "myred">ExtSpillGate</span></td>
    <td class="tcenter"><span class = "myred">0x3070'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ext spill gate.</td>
  </tr>
  <tr>
    <td><span class = "myred">ExtCCRst</span></td>
    <td class="tcenter"><span class = "myred">0x3080'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ext counter reset</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter"><span class = "myred">0x3090'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext busy.</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter"><span class = "myred">0x30A0'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext rsv2.</td>
  </tr>
  </tbody>
</table>

### Function of each block on HUL Scaler

**Trigger Manager (TRM)**<br>
Same with HUL RM.

**Scaler (SCR)**<br>
The scaler is the main function of the HUL Scaler. Each scaler unit synchronizes the input signal at 300 MHz, then performs edge detection and increments the counter by one at that edge timing. The minimum width of the input pulse is 3.5 ~ 4.0 ns, and the minimum separation between two pulses is about the same. The counter is 28-bit long and returns to 0 after going around. The scaler is divided into 4 blocks of 32ch, and they are enable/disabled  in the `EnableBlock` register. If the corresponding bit is high/low, the entire block (32ch) is enabled/disabled. The scaler is reset to zero by the hard reset `ExtCounterReset` (controlled by NIMIN, IOM) or the soft reset `CounterReset` is asserted. The hard reset is enabled/disabled by the block with `EnableHdrst`. If this bit is 1, the counter will be 0 at the timing of the hard reset. The operation is undefined when a reset is entered within 100 ns of the trigger. The data may be returned, but contain unintended values.

The entire scalers are enabled/disabled by `ExtSpillGate` (NIMIN, controlled by IOM). The scaler is only incremented when the spill gate is high. The Spill gate is 1 by default, and may be assigned to an NIM input port.
**I/O Manager (IOM)**<br>
IOM is the same with HUL RM, with additional NIM inputs `ExtSpillGate` and `ExtCCRst`. No change NIMOUTs.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register label </span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>Signals available for NIMOUT</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busy, meaning only the busy status of the module. J0 bus busy or ExtBusy are not included.</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusy, including the module busy, J0 bus busy and ExtBusy. Usage of this signal assumes that HUL is the bus master, and HRM returns the
 same busy to master trigger module (MTM).</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRM L1 trigger as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRM L2 trigger as HRM has received.</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRM Clear as HRM has received.</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRM Reserve 1 as HRM has received.</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRM outputs Spill Number Increment</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gate of DCT</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>ch 8 of DIP SW2</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHz clock</td>
  </tr>

  <tr><td class="tcenter" colspan=3><b>NIMIN ports available</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>If this register is set, the default assignment are done for signal lines (see next table).</td>
  </tr>
</tbody>
</table>

IOM default assignments as listed below.

<table class="vmgr-table">
  <thead><tr>
  <th class="nowrap"><span class="mgr-20">NIM output ports</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Signal</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10">default</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr><span class="myred">
    <td class="myred">ExtSpillGate</td>
    <td>Reg_i_default</td>
    <td class="myred">1</td></span>
  </tr>
  <tr><span class="myred">"
    <td class="myred">ExtCounterReset</td>
    <td>Reg_i_nimin2</td>
    <td class="myred">NIMIN2</td></span>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### Switch and LED on HUL Scaler

**DIP SW2**<br>
The same with HUL RM.

**LED**<br>
The same with HUL RM.

### DAQ behavior of HUL Scaler

The data flow is shown in [Figure](#SCALERDAQ). If HRM is installed and DIP SW2 ch2 (mezzanine HRM) is ON, RVM and SCR contributes to the event; otherwise, only SCR contributes. The *number of word* of header2 contains the total of SCR and RVM data.

![daq-scaler-fw](daq-scaler-fw.png "DAQ data path of HUL Scaler firmware"){: #SCALERDAQ width=90% }

**Module Busy timing**<br>
HUL Scaler has the same definition of module BUSY with HUL RM, except the time length is different (HUL Scaler: 210 ns, HUL RM: 160 ns) due to the difference of the system clock frequency.

**Data structure**<br>**Header word**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF4CA1                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "00000"    |         Number of word (11-bit)         |
```
"Number of word" indicates the number of words contained in the data body, not including the header.
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
If *HRM* exist is 1, ch2 of DIP SW2 is on, meaning HRM was installed. This means that data body has a RVM word.
*Tag* is the 4-bit Tag information from TRM. The lower 3 bits are the lower 3 bits of the RM Event Number,
and the 4th bit is the least significant bit of the RM spill number. *Self counter* is a local event number that is incremented each time an event is forwarded. Starts with 0.

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```
*Lock* means the RM lock bit, must be 1. *SNI* means Spill Number Increment, which becomes 1 at the change of the Spill Number (not tested if it is true). *Spill Num* is the spill number, and *Event Num* is the event number, received by HRM, respectively.
```
SCR word
|  SCR block (4-bit)   |                            Counter (28-bit)                           |
```
SCR Block indicates which input block the word belongs to. There is no field to indicate the channel in the SCR words; they are aligned from lower to higher channel numbers in the input block.
<!-- ![data-scaler-fw](data-scaler-fw.png "DAQ data structure of HUL Scaler firmware"){: #DATASCR width=90% } -->

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">SCR block bits</span></th>
    <th class="nowrap"><span class="mgr-10">Input block</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">0x8</td>
    <td class="tcenter">Main input port U</td>
  </tr>
  <tr>
    <td class="tcenter">0x9</td>
    <td class="tcenter">Main input port D</td>
  </tr>
  <tr>
    <td class="tcenter">0xA</td>
    <td class="tcenter">Mezzanine U</td>
  </tr>
  <tr>
    <td class="tcenter">0xB</td>
    <td class="tcenter">Mezzanine D</td>
  </tr>
  </tbody>
</table>

## HUL MH-TDC

HUL MH-TDC is a firmware that adds a multi-hit TDC function to  HUL RM. Since the HUL MH-TDC has many functions in common with the HUL RM, only the differences will be mentioned.

**Firmware ID and current version**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>ID</td>
    <td class="tcenter">0x30CC</td>
  </tr>
  <tr>
    <td>Major version</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x04</td>
  </tr>
  </tbody>
</table>

**Version history**
Similar to HUL RM version history. Marked <span class="myred">red</span> if different.
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Version</span></th>
    <th class="nowrap"><span class="mgr-10">Release date</span></th>
    <th>Modifications</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.0</td>
    <td class="tcenter">2016.12.23</td>
    <td>Initial release</td>
  </tr>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2017.01.15</td>
    <td>RVM data header changed from 0x9C to 0xF9.</td>
  </tr>
  <tr>
    <td class="tcenter">v1.2</td>
    <td class="tcenter">2017.01.27</td>
    <td>Vivado更新 2016.2 => 2016.4。<span clas="myred">Block buffer changed from BuildIn FIFO to BRAM with depth 4096. EventBuffer depth changed from 2048 to 4096, and pgfull to be 4058. TDC block channel buffer changed from dispersive RAM to BRAM. </span>TRM middle buffer changed from disperse RAM to BRAM. The depth changed from 128 to 256. Prog Full threshold introduced. RVM middle buffer depth is also changed to 256 (128?).
 </td>
  </tr>
  <tr>
    <td class="tcenter">v1.4</td>
    <td class="tcenter">2017.05.09</td>
    <td>Solved the problem of not responding to Clear (BUSY stays standing).</td>
  </tr>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter">-</td>
    <td>Solved the problem that HRM hangs when Clear is entered. (Replaced by v1.6 without release.)</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">-</td>
    <td><span class="myred">Addressed the issue that the event ID shifts when max multihit (16 hit / ch) is reached once. (un-released)</span> </td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2017.08.22</td>
    <td><span class="myred">Bug fix of event sequence in FPGA</span></td>
  </tr>
  <tr>
    <td class="tcenter">v1.8</td>
    <td class="tcenter">2017.12.19</td>
    <td>Standardlized reset sequence. Bit 24 of Header3 is now  indicating whether HRM exists (to be exact, whether DIP2 is ON). <span class="myred">Bug fix of rare broken data in high count-rate. Number of words in Header2 changed the width from 11-bit to 12-bit.</span></td>
  </tr>
  <tr>
    <td class="tcenter">v1.9</td>
    <td class="tcenter">2018.02.02</td>
    <td>>Solved the bug that the event tag coming from the J0 bus was latched too early and the event number on the HRM side deviated by 1.
    Solved the bug that data comes back out side of the search window (common to Mezzanine HR-TDC).</td>
  </tr>
  <tr>
    <td class="tcenter">v2.x</td>
    <td class="tcenter"> - </td>
    <td>un-released</td>
  </tr>
  <tr>
    <td class="tcenter">v3.4</td>
    <td class="tcenter">2021.08.01</td>
    <td>Added FMP and SDS. Installed Builder bus. Changed the structure of Local bus.</td>
  </tr>
  </tbody>
</table>

**Overview of Module functions**<br>
HUL MH-TDC is implemented for Mezzanine slot D and on-board input ports, in addition to Mezzanine slot U configured for HRM. DCR v1 (v2) is assumed to be installed in the Mezzanine slot(s), implementing TDC up to 128 channels. It is also possible to mount HRM (instead of DCR) on Mezzanine slot U to become a J0 bus master like HUL RM firmware. In this case, the Ch 0-31 assigned to slot U is deleted from the data.

The MH-TDC implements a 300 MHz 4-phase clock TDC with a 1-bit precision of 0.83 ns. Both leading and trailing edges can be detected, the length of time that can be traced back from the trigger is 13.7 us, and the timing resolution is 300 ps (σ).

![mhtdc-fw](mhtdc-fw.png "HUL MH-TDC FWの構造。"){: #MHTDCFW width=90% }

### Register map of HUL MH-TDC

The following is a map dedicated to HUL MH-TDC. The difference in the signal and address from HUL RM is marked as <span class="myred">red</span>. Signal names and the address are defined in RegisterMap.hh and namespace in the software package.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Register</span></th>
    <th class="nowrap"><span class="mgr-10">Address</span></th>
    <th class="nowrap"><span class="mgr-10">Operation</span></th>
    <th class="nowrap"><span class="mgr-10">bit width</span></th>
    <th>Description</th>

  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>Selects trigger port in TRM</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>On/Off od the DAQ gate.</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Write to this address asserts a soft reset to EVB, and self event counter in Event builder becomes zero. (Don't care about the register value.)</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: TDC (module ID = 0x02)</b></td></tr>
  <tr>
    <td><span class="myred">EnableBlock</span></td>
    <td class="tcenter"><span class="myred">0x2000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Enable/Disable the input blocks. High for enable. The bit corresponds to:<br>
      1st bit：Main input port U <br>
      2nd bit：Main input port D <br>
      3rd bit：Mezzanine U <br>
      4th bit：Mezzanine D </td>
  </tr>
  <tr>
    <td><span class="myred">PtrOfs</span></td>
    <td class="tcenter"><span class="myred">0x2010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Internal use. Do not touch.</td>
  </tr>
  <tr>
    <td><span class="myred">WindowMax</span></td>
    <td class="tcenter"><span class="myred">0x2020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>The upper limit of the time window to search for hits from the Ring buffer. 1bit is equivalent to 6.666... ns. Detail is described later.</td>
  </tr>
  <tr>
    <td><span class="myred">WindowMin</span></td>
    <td class="tcenter"><span class="myred">0x2030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>The lower limit of the time window to search for hits from the Ring buffer. 1bit is equivalent to 6.666... ns. Detail is described later.</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter"><span class="myred">0x3000'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT1.</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter"><span class="myred">0x3010'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT2.</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter"><span class="myred">0x3020'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT3.</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter"><span class="myred">0x3030'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Determines what to send to NIMOUT4.</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter"><span class="myred">0x3040'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL1.</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter"><span class="myred">0x3050'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to ExtL2.</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter"><span class="myred">0x3060'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext clear.</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter"><span class="myred">0x3070'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext busy.</td>
  </tr>
  <tr>
    <td>ExtRsv2</td>
    <td class="tcenter"><span class="myred">0x3080'0000</span></td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Determines which NIMIN is connected to Ext rsv2.</td>
  </tr>
  </tbody>
</table>

### Function of each block on HUL MH-TDC

**Trigger Manager (TRM)**<br>
Same with HUL RM.

**Multi-Hit TDC (MH-TDC)**<br>
This is the main function of this firmware. This MH-TDC uses a 4-phase clock to create a pseudo 1.2 GHz. The multi-hit TDC block shown in [Figure](#MHTDCDAQ) contains three components: the TDC unit, the ring buffer, and the channel buffer. The TDC unit measures time at pseudo 1.2 GHz and performs hit detection. The TDC unit has a time resolution of 300 ps (σ) and a minimum detectable pulse width of approximately 4 ns. The detected hit information is saved in the ring buffer. The length of the ring buffer is 13.7 us, and the write / read pointer of the ring buffer corresponds to the course count. The Ring buffer is driven by a 150 MHz clock, so the course count is 6.666 ... ns accurate.

When a L1 trigger is detected,  ring buffer read out starts. The range to search for hits is set by `WindowMax`  and ` WindowMin` registers. These registers are 11-bit integer with the course count resolution for 1-bit. The Hits which do not fall within the range will not be written to the channel buffer. BUSY is asserted while searching for hit information from the Ring buffer.

The maximum number of hits allowed for 1ch / event is 16 hits,  when transferred from channel buffer to block buffer in reverse time order. Any hits more than that will be discarded and the overflow bit will be set.

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>Multi-hit TDC specfication</b></td></tr>
  <tr>
    <td>TDC resolution</td>
    <td class="tcenter">0.833... ns</td>
  </tr>
  <tr>
    <td>coarse count resolution</td>
    <td class="tcenter">6.66... ns</td>
  </tr>
  <tr>
    <td>Ring buffer length</td>
    <td class="tcenter">13.8 us</td>
  </tr>
  <tr>
    <td>timing resolution</td>
    <td class="tcenter">300 ps (σ) *measured</td>
  </tr>
  <tr>
    <td>minimum pulse width</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>double hit resolution</td>
    <td class="tcenter">~7 ns</td>
  </tr>
  <tr>
    <td>maximum hits/ch/event</td>
    <td class="tcenter">16</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**
IOM is the same with HUL RM.
<table class="vmgr-table">
  <thead><tr>  <thead><tr>
     <th class="nowrap"><span class="mgr-20">Register label</span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>Signals available for NIMOUT</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busy, meaning only the busy status of the module. J0 bus busy or ExtBusy are not included.</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusy, including the module busy, J0 bus busy and ExtBusy. Usage of this signal assumes that HUL is the bus master, and HRM returns the same busy to master trigger module (MTM).</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRM L1 trigger as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRM L2 trigger as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRM Clear as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRM Reserve 1 as HRM has received. </td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRM Spill Number Increment as HRM has received</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gate in DCT</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>ch 8 of DIP SW2</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHz clock</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>NIMIN ports available</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>If this register is set, the default assignment are done for signal lines (see next table).</td>
  </tr>
</tbody>
</table>

IOM default assignments as listed below.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM output ports</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Signal</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10">default</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
</tbody>
</table>

### Switch and LED on HUL MH-TDC

**DIP SW2**<br>
The same with HUL RM

**LED**<br>
The same with HUL RM

### DAQ operation

Data flow is shown in [Figure](#MTDCDAQ). If HRM is installed and DIP SW2 ch2 (mezzanine HRM) is ON, RVM and TDC contributes to the event; otherwise, only TDC contributes. The "number of word" of header2 contains the total of TDC and RVM data.

![daq-mhtdc-fw](daq-mhtdc-fw.png "DAQ data flow of HUL MH-TDC firmware"){: #MTDCDAQ width=90% }

**Module Busy timing**<br>
HUL MH-TDC has the same definition of module BUSY with HUL RM, except the time length is different (HUL MH-TDC: 210 ns, HUL RM: 160 ns) due to the difference of the system clock frequency. In addition it as the Sequence busy, which is ORed.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY type</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY length</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>Asserted with a fixed length since the L1 trigger is detected.</td>
  </tr>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">Dependes on the search window length.</td>
    <td>Depends on `WindowMax`-`WindowMin`: BUSY while hits are searched in the Ring buffer.</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>BUSY is output when the block buffer is full. It is asserted when the L1 trigger rate exceeds the data processing speed of the subsequent circuit. This happens when TCP transfer cannot catch up and is practically equivalent to SiTCP full.</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>TCP buffer of SiTCP becomes Full. It is asserted when the amount of data that the Event Builder is trying to send is large for the network bandwidth.</td>
  </tr>
</tbody>
</table>

**Data structure**<br>**Header word**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF30CC                                         |

Header2 (event size)
|       0xFF       |      0x00       |    "0000"    |         Number of word (12-bit)          |
```
*Number of word* indicates the number of words contained in the data body, not including the header.
```
Header3 (event number)
|       0xFF       | HRM exist | "000" | Tag (4-bit) |         Self counter (16-bit)           |
```
If *HRM* exist is 1, ch2 of DIP SW2 is on, meaning HRM was installed. This means that data body has a RVM word.
*Tag* is the 4-bit Tag information from TRM. The lower 3 bits are the lower 3 bits of the RM Event Number,
and the 4th bit is the least significant bit of the RM spill number. *Self counter* is a local event number that is incremented each time an event is forwarded. Starts with 0.

**Data body**
```
RVM word
|  0xF9   | "00"  | Lock  | SNI  | Spill Num (8-bit) |            Event Num (12-bit)           |
```
*Lock* means the RM lock bit, must be 1. *SNI* means Spill Number Increment, which becomes 1 at the change of the Spill Number (not tested if it is true). *Spill Num* is the spill number, and *Event Num* is the event number, received by HRM, respectively.
```
TDC word
|  Magic word (8-bit)  |  "0" + Ch (7-bit)  | "00" |            TDC (14-bit)                   |
```
The "Magic word" is defined as follows.
<ul>
  <li> 0xCC Leading
  <li> 0xCD Trailing
</ul>

*Ch* is the channel number starts with 0, up to 127. Refer to Chapter 1 for Channel-Input port assignment. *TDC* is the 14-bits in the least.

<!-- ![data-mhtdc-fw](data-mhtdc-fw.png "Data structure of HUL MH-TDC firmware"){: #DATATDC width=90% } -->

## Mezzanine HR-TDC  and HUL HR-TDC BASE

This section describes the firmware inside the Mezzanine HR-TDC and the firmware to control it. Mezzanine HR-TDC is a firmware that implements the functions up to the block buffer in HUL MH-TDC, and HUL HR-TDC BASE implements the subsequent function to manage the entire DAQ such as event builder and trigger manager. Therefore, the Mezzanine HR-TDC cannot perform complicated operations. It transfers the measurement data to the HUL in response to the Trigger (Common stop). The control system is rather complicated because it has two FPGAs. Mezzanine HR-TDC has features that other firmware does not have, such as a tapped-delay-line calibration LUT and DDR communication for data transfer. This section describes these functions and control methods.

**Mezzanine HR-TDC ID and current version**<br>
Previous firmware has two versions with/without the trailing edge measurements;  the new firmware is integrated into one. The edge to be measured is selected by the register.

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>ID number</td>
    <td class="tcenter">0x80cc</td>
  </tr>
  <tr>
    <td>Major version</td>
    <td class="tcenter">0x05</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x00</td>
  </tr>
  </tbody>
</table>

**Version history**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Version</span></th>
    <th class="nowrap"><span class="mgr-10">Release date</span></th>
    <th>Changes</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v2.5</td>
    <td class="tcenter">2017.12.19</td>
    <td>initial release with leading edge measurements.</td>
  </tr>
  <tr>
    <td class="tcenter">v2.6</td>
    <td class="tcenter">2018.02.02</td>
    <td>Resolved an issue of returning data out of the search window.</td>
  </tr>
  <tr>
    <td class="tcenter">v3.2</td>
    <td class="tcenter">2017.12.19</td>
    <td>Initial release for leading+trailing edge measurements</td>
  </tr>
  <tr>
    <td class="tcenter">v3.3</td>
    <td class="tcenter">2018.02.02</td>
    <td>Resolved an issue of returning data out of the search window.</td>
  </tr>
  <tr>
    <td class="tcenter">v4.5</td>
    <td class="tcenter">2021.08.01</td>
    <td>Installation of SEM and XADC. Installation of Builder bus. Register selection of the measurement edges.<br>
    Minor bug correction on XDC (the same functions with v4.3).<br>
    TDC samplning clock and system clock signals are 520 MHz ans 130 MHz up to this version.
    </td>
  </tr>
    <tr>
    <td class="tcenter">v5.0</td>
    <td class="tcenter">2023.01.17</td>
    <td>Implemented trigger signal output from mezzanine card.<br>
    Modified the local bus bridge. Mezzanine FW v5.0 is not compatible with HUL HRTDC BASE firmware prior to v3.7.<br>
    TDC samplning clock and system clock signals are changed to 500 MHz ans 125 MHz, respectively.
    </td>
  </tr>
  </tbody>
</table>

**Overview of module function**<br>
Block diagram of Mezzanine HR-TDC and HUL HR-TDC BASE are shown in [Figure](#BLOCKHRTDC), which is drawn in some detail, since the control system is more complicated than other firmware. In the HR-TDC system, BCT exists in each FPGA, and the mezzanine HR-TDC side controls registers with two-step access through the BsuBrige on the BASE side. There is a dedicated C ++ function for controlling the Mezzanine side through BisBridge, which will be explained in more detail in the software section.

The system is divided into two subsystems: Mezzanine HR-TDC which only measures time, and BASE which manages event build, trigger control and IOs. Trigger information is managed by the Trigger Manager (TRM) like any other modules. Only the level 1 trigger that is issued by TRM is sent to the Mezzanine HR-TDC. This signal works as a common stop for the mezzanine HR-TDC. The operation as TDC is equivalent to HUL MH-TDC. Only the time resolution is higher. The ring buffer length for recording hits is 15.7 us, and the time resolution is 25 ps (σ) (for common stop) and 20 ps (σ) (difference between channels).

In order to transfer data from mezzanine to BASE at a high speed, 5 signal lines are used. Since the transfer includes the control bits, not the full bandwidth is available; the time required for transfer of one word (32 bit) is 8 ns (i.e ~4 Gbps). The DDR receiver on the BASE side needs to be initialized once after the power is turned on. The initialization method is also described in the software section. The TDC base receives the data and prepares to pass it to the event builder.

![blocks-hrtdc-fw](blocks-hrtdc-fw.png "Block diagram of HUL HR-TDC formware"){: #BLOCKHRTDC width=90% }

### Details of Mezzanine HR-TDC

**The principle of high-timing resolution measurements**<br>
This firmware uses a time measurement method called the tapped-delay-line (TDL) method. The concept of TDL is shown in [Figure](#TDL). TDL is consist of linear connections of fine delay elements (dTs) and flip-flops (FFs); the time information is interpolated by observing how many FFs has the input signal edge run through. The gray square in [Figure](#TDL) shows the delay elements, and the D-FF array takes a snapshot of the FF on/off status in every clock edges. The delay element is referred as tap hereafter. The clock for taking the snapshots is 500 MHz (2.0 ns), so if the number of taps is known where the input signal pulse went through during 2.0 ns, the delay amount per tap (dT) is known. Ignoring the statistical fluctutations, dT equials to "2000 (ps) / maximum tap number of reach", which is the time for the TDC 1-bit. However, each delay element in FPGA HR-TDCs has different tap delays; instead of the static calibration, a dynamic calibration that converts all tap numbers into time is required. Here, the tap number is called *fine-count*, and the value converted from fine count to the physical time is called *estimator*.

![hrtdc-tdl](hrtdc-tdl.png "Schematics of Tapped-delay-line"){: #TDL width=90% }

**Time calibration**<br>
Figure below shows the procedure for generating an estimator. First, a fine-count histogram has to be generated for a time-uncorrelated white noise spectrum as an input. The histogram weight for the each fine-count bin is proportional to the corresponding time delay of the bin.  Once the histogram is ready, integrate the histogram counts of each bin up to the N-th to obtain the estimator; i.e. if the number of counts in the i-th bin is $`w_i`$, the N-th estimator is:
```math
E_{n} = w_{n}/2 + \sum^{n-1}_{0}(w_{i})
```
In the FPGA, required a mechanism to generat a fine-count histogram and a circuit to convert each hit from its fine-count to the estimator for output.  As the signal source, the easiest  is to use the detector signal to generate a fine count histogram. (The detector signal must be time-uncorrelated and random.) Histogram generation is independent of DAQ, and all input signals may be automatically filled into the histogram. However, this method requires a wait until the event accumulates to a fixed integra (e.g. 0x7ffff), and only available to the channel with a detector is connected. Therefore, a method to generate the histogram using a clock is prepared.
A more practical method is to connect the calibration clock inside the FPGA to all input lines. This clock is adjusted to make a slight phase shift to the system clock which samples the TDL. In principle, the edge of the calibration clock can sweep the time in 1ps resolution. With the method of calibration clock, a histogram can be generated in tens of ms. It is assumed that the module is calibrated after the power is turned on, or at the beginning of each RUN.

![hrtdc-estimator](hrtdc-estimator.png "Procedure to generate estimators from fine count histogram."){: #ESTIMATOR width=90% }

**Calibration block impremented and operation**<br>
The time calibration system is implemented by two RAMs. As shown in [Figure](#HRTD CLUT), the fine-count is simultaneously fed into the two RAMs. One RAM generates a histogram. When the prefixed 0x7ffff event counts are accumulated, the RAM may be converted to the estimator mode, waiting for the swap. In the estimator mode, the RAM converts from a fine-count to an estimator. Since the raw estimator is 19 bits wide, which is too fine, and is discarded the lower 8 bits and make it 11 bits wide for output. The RAM swap is either automatic when one is ready, or  manually switched. The behaviour is controled in  `Controll::AutoSw` and` ReqSwitch` . If `Controll::AutoSw` is 1, it will switch automatically; if it is 0 and ` ReqSwitch` is written, RAM will switch manually. Automatic switching is used when constant update of the RAM is necessary using the detector signal during the RUN.

Also, extraction of the fine-count without being converted to the estimator is possible. Set `Controll::Through` to 1 and the fine-count will appear directly.

![hrtdc-lut](hrtdc-lut.png "Swap pattern of Estimator look up table (LUT)."){: #HRTDCLUT width=90% }

**HR-TDC system**<br>
This section is a summary of the HR-TDC functions inside the Mezzanine HR-TDC. In this firmware, the length of TDL is 192 taps, which is rather too fine. Three taps are combined into one, conbined to 64 effective taps. Therefore, the maximum fine-count is 63. According to the measurements, approximately 55 taps are reached in 2.0 ns. The fine-count is passed to the clock area of 125 MHz, converted to the estimator, and then written to the ring buffer together with the hit bit. As shown in [Figure](#HRTDCBLOCK), estimator (11bit) + semi-coarse count (2bit) + coarse count (11bit) gives a data length of 24bit. After that, the event is partially built in mezzanine HR-TDC and transferred to the HUL side. Approximately,
Time (ns) = TDC value / 2048 / ClkFreq, where  2048 is the estimator's maximum and ClkFreq=0.500 GHz (FW v5.0 and later) or 0.520 GHz (up to FW v4.5) is the clock frequency.
Use the TDC calibrator for a more accurate time conversion.
The implementation after the Ring buffer is the same as in MH-TDC. When L1 trigger (common stop) is detected, the ring buffer is read and transfered.
The range to search for hits may be set by  `WindowMax`  and ` WindowMin` registers. These registers has the resolution of the 11-bit course count. Hits that do not fall within this range will not be written to the channel buffer. BUSY signal is asserted while searching for the hit information from the Ring buffer. Eventhough the structure is the same, the coarse count resolution is different from MH-TDC, since the system clock frequency is different.

The maximum number of hits for 1ch / event is placed when collecting data from channel buffer to block buffer. Curently, it is 16 hits, and data in the earlier time of TDC is discarted if more hits are recorded in the channel buffer, and the overflow bit is set.

**Trigger output**<br>
Trigger signal can be output from HR-TDC from Version 5.0.
The hit bits for all channels are logically summed and output.
When trigger output is not used, it can be masked for each channel by TrigMask register.

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>High-resolution TDC specs</b></td></tr>
  <tr>
    <td>TDC resolution</td>
    <td class="tcenter">~30 ps</td>
  </tr>
  <tr>
    <td>coarse count resolution</td>
    <td class="tcenter">8.0 ns</td>
  </tr>
  <tr>
    <td>Ring buffer length</td>
    <td class="tcenter">16.3 us</td>
  </tr>
  <tr>
    <td>timing resolution</td>
    <td class="tcenter">20 ps (σ) *measured</td>
  </tr>
  <tr>
    <td>minimum pulse width</td>
    <td class="tcenter">~2 ns</td>
  </tr>
  <tr>
    <td>double hit resolution</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>maximum hit/ch/event</td>
    <td class="tcenter">16</td>
  </tr>
  </tbody>
</table>

![hrtdc-tdcblock](hrtdc-tdcblock.png "Block diagram of HR-TDC"){: #HRTDCBLOCK width=90% }

### Register map of Mezzanine HR-TDC

This section summarizes the registers of the Mezzanine HR-TDC. The registers described here belong to `namespace HRTDC_MZN` in` RegisterMap.hh`. Since the Mezzanine HR-TDC is accessed by bus bridging through BusBridge, it cannot be specified directly by the RBCP address.
**Since Version 5.0, the address range has been expanded from 12-bit width to 16-bit width by modifying the local bus bridge.**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register name</span></th>
    <th class="nowrap"><span class="mgr-20">Address</span></th>
    <th class="nowrap"><span class="mgr-10">Operation</span></th>
    <th class="nowrap"><span class="mgr-10">Bit width</span></th>
    <th>Description</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: DCT (module ID = 0x0)</b></td></tr>
  <tr>
    <td>TestMode</td>
    <td class="tcenter">0x0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>A mode that outputs a test pattern from the DDR transmitter to initialize the DDR receiver on the HUL side. Necessary for module initialization at power-on, used inside the distributed C++ software ddr_initialize.</td>
  </tr>
  <tr>
    <td>ExtraPath</td>
    <td class="tcenter">0x0010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>When this bit is set, the signal input path switches from the input port to the calibration clock. Used to generate the LUT for the estimator with a calibration clock.</td>
  </tr>
  <tr>
    <td>Gate</td>
    <td class="tcenter">0x0020</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DAQ gate. If it is 1, common stop is input to HR-TDC.</td>
  </tr>
  <tr>
    <td>EnBlocks</td>
    <td class="tcenter">0x0030</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">2</td>
    <td>Enable the leading / trailing measurement block. The first bit is the leading block and the second bit is the trailing block. Since the default is 0, this bit must be set.</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: TDC (module ID = 0x01)</b></td></tr>
  <tr>
    <td>Control</td>
    <td class="tcenter">0x1010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>A register for changing the operation of HR-TDC. The following three bits exist.<br>
        Through 	(0x1)<br>
        AutoSw 	  (0x2)<br>
        StopDout 	(0x4)<br>
        If Through is 1, the fine-count is transferred without being converted to the estimator.
        If AutoSw is 1, the RAM will be swapped as soon as a new LUT is ready on the other RAM.
        If Stop Dout is 1, the stop data is also transferred as one word without subtracting from the common stop inside the FPGA.</td>
  </tr>
  <tr>
    <td>ReqSwitch</td>
    <td class="tcenter">0x1020</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>If AutoSw is 0, the estimator RAM will be swapped when this register is accessed. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1030</td>
    <td class="tcenter">R</td>
    <td class="tcenter">1</td>
    <td>If this bit is 1, the next estimator LUT is ready on the alternative RAM.</td>
  </tr>
  <tr>
    <td>PtrOfs</td>
    <td class="tcenter">0x1040</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Internal use. Do not touch. </td>
  </tr>
  <tr>
    <td>WindowMax</td>
    <td class="tcenter">0x1050</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>The upper limit of the time window to search for hits from the Ring buffer. 1bit is equivalent to 8.0 ns. See MH-TDC for details.</td>
  </tr>
  <tr>
    <td>WindowMin</td>
    <td class="tcenter">0x1060</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>The lower limit of the time window to search for hits from the Ring buffer. 1bit is equivalent to 8.0 ns. See MH-TDC for details.</td>
  </tr>
  <tr>
    <td>TrigMask</td>
    <td class="tcenter">0x1070</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">32</td>
    <td>Trigger output mask for each channel. Each bit corresponds to the channel. If 0 is set, the target channel is masked. E.g., if you want to mask channel 0, set 0xFFFF'FFFE. </td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: SDS (module ID = 0xC)</b></td></tr>
  <tr>
    <td>SdsStatus</td>
    <td class="tcenter">0xC000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">8</td>
    <td>Obtains the status of SDS module.</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>XadcDrpMode</td>
    <td class="tcenter">0xC010</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>DRP mode select for XADC.<br>
        <ul>
          <li> 0x0: Read mode</li>
          <li> 0x1: Write mode</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>XadcDrpAddr</td>
    <td class="tcenter">0xC020</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>DRP address for XADC.</td>
  </tr>
  <tr>
    <td>XadcDrpDin</td>
    <td class="tcenter">0xC030</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">16</td>
    <td>DRP input data for XADC</td>
  </tr>
  <tr>
    <td>XadcDrpDout</td>
    <td class="tcenter">0xC040</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>Obtains DRP output data from XADC.</td>
  </tr>
  <tr>
    <td>XadcExecute</td>
    <td class="tcenter">0xC050</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Execute DRP access to XADC. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td class="tcenter"> </td>
    <td> </td>
  </tr>
  <tr>
    <td>SemCorCount</td>
    <td class="tcenter">0xC0A0</td>
    <td class="tcenter">R</td>
    <td class="tcenter">16</td>
    <td>Obtain the number of SEU corrected by SEM.</td>
  </tr>
  <tr>
    <td>SemRstCorCount</td>
    <td class="tcenter">0xC0B0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Reset SemCorCount to zero. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td>SemErroAddr</td>
    <td class="tcenter">0xC0C0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">40</td>
    <td>Address input for inject_address of SEM.</td>
  </tr>
  <tr>
    <td>SemErroStrobe</td>
    <td class="tcenter">0xC0D0</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Sends pulse to inject_strobe of SEM. (Don't care about the register value.)</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: BCT (module ID = 0xE)</b></td></tr>
  <tr>
    <td>Reset</td>
    <td class="tcenter">0xE000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Asserts module reset signal from Bus Controller, and initializes all modules except SiTCP. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td>Version</td>
    <td class="tcenter">0xE010</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Reads Firmware ID and versions. Multiple byte must be read out.</td>
  </tr>
  <tr>
    <td>Reconfig</td>
    <td class="tcenter">0xE020</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Sends Low to PROG_B_ON to re-configure FPGA. SiTCP connection will be closed, and may be reconnected in a few seconds. (Don't care about the register value.)</td>
  </tr>
  </tbody>
</table>

### Switch and LED on Mezzanine HR-TDC board

**DIP SW functions**<br>
This function is assigned to the 4-bit DIP switch on the board.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">switch number</span></th>
    <th class="nowrap"><span class="mgr-30">function</span></th>
    <th class="nowrap"><span class="mgr-10">details</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">-</td>
    <td>Reserved</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">-</td>
    <td>Reserved</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">-</td>
    <td>Reserved</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">-</td>
    <td>Reserved</td>
  </tr>
</tbody>
</table>

**LED function**<br>
The Mezzanine HR-TDC does not have a LED available to the user. The red LED lights when the FPGA is configured.

**Module Busy timings**<br>
The definition of BUSY for Mezzanine HR-TDC is the OR of the BUSY signals listed below.  In addition, BUSY of HUL HR-TDC BASE is also ORed. Normally, the BUSY length is equal to that for "Sequence busy".

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY type</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY length</span></th>
    <th class="nowrap"><span class="mgr-10">description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">Depends on the search window width</td>
    <td>Depends on  WindowMax-WindowMin: BUSY while hits are searched in the Ring buffer</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>BUSY is output when the block buffer is full. It is asserted when the L1 trigger rate exceeds the data processing speed of the subsequent circuit. This happens when TCP transfer cannot catch up and is practically equivalent to SiTCP full.</td>
  </tr>
</tbody>
</table>

### Details of HUL HR-TDC BASE

HUL HR-TDC BASE has almost the same structure with MH-TDC except for DDR receiver and BusBridge. Unlike MH-TDC, HRM cannot be installed. Therefore, it cannot become a J0 bus master.

The DDR receiver needs to be initialized after the power is turned on; other than that there is no special action necessary. The BusBridge is provided for mezzanine slot U and D independently. BusBrige module bridges the BCT on the HUL side and the BCT on the mezzanine side. From the BCT on the HUL side, the BusBridge looks as a local module, and from the mezzanine side, it looks like an external link. Two actions are required to access the mezzanine. In the first action, the BCT on HUL stores the read/write command, the mezzanine local address, and the register value to `BBP::Txd`, a register located inside BusBrige.  The second action sends signal to `BBP::Exec` for the brige action; BusBrige will start the communicating procedure with the mezzanine. Whether the BCT on the mezzanine side is driven by writing or by reading is determined by the command value specified in the first action. In the read mode, the value from the specified address will appear on `BBP::Rxd`. BusBrige will not respond to the BCT on HUL until the communication process is completed correctly. <span class = "myred"> Therefore,  the BCT on HUL will be deadlock, if `BBP::Exec` is called without a mezzanine HR-TDC card installed. </span> If this occurs, `BCT::Reset` will not be called, and SiTCP Reset will be necessary. C++ functions for BusBrige control are grouped in `BctBusBridgeFunc.cc`. Details will be given in Chapter 6.

**HR-TDC BASE ID and current version**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>ID</td>
    <td class="tcenter">0x80eb</td>
  </tr>
  <tr>
    <td>Mojor version</td>
    <td class="tcenter">0x04</td>
  </tr>
  <tr>
    <td>Minor version</td>
    <td class="tcenter">0x01</td>
  </tr>
  </tbody>
</table>

**Version history**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Version</span></th>
    <th class="nowrap"><span class="mgr-10">Release date</span></th>
    <th>Changes</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.5</td>
    <td class="tcenter">2017.12.19</td>
    <td>Initial release</td>
  </tr>
  <tr>
    <td class="tcenter">v1.6</td>
    <td class="tcenter">-</td>
    <td>not released</td>
  </tr>
  <tr>
    <td class="tcenter">v1.7</td>
    <td class="tcenter">2018.02.02</td>
    <td>Solved the issue that the event tag coming from the J0 bus was latched too early and the event number on HRM  is deviated by 1. Repaired the issue that BCT hangs when calling BCT::Reset.</td>
  </tr>
  <tr>
    <td class="tcenter">v3.7</td>
    <td class="tcenter">2021.08.01</td>
    <td>Added SDS and  FMP.  Installed Builder bus. Change of BCT structure.</td>
  </tr>
  <tr>
    <td class="tcenter">v4.0</td>
    <td class="tcenter">2023.01.17</td>
    <td>Compatible with Mezzanine HR-TDC v5.0. Mezzanine HR-TDC v4.5 and earlier versions are not supported.</td>
  </tr>
  <tr>
    <td class="tcenter">v4.1</td>
    <td class="tcenter">2023.02.24</td>
    <td>Bug-fixed version of HUL HRTDC BASE v4.0. Sometimes, v4.0 does not work correctly.</td>
  </tr>
  </tbody>
</table>

### Register map of HUL HR-TDC BASE

The following is the register map of HUL HR-TDC BASE, defined in RegisterMap.hh as namespace HRTDC_BASE.
Some of the names are identical with ones in Mezzanine; specify the namespace. Some registers are missing due to the un-support of HRM in IOM.

 `RegisterMap.hh` for this firmware contains the global variables `kEnSlotup` and `kEnSlotDown` at the beginning.
 These are flags indicating which slot the mezzanine HR-TDC is installed. Set to false if Mezzanine HR-TDC is not installed.
 These are variables that are not necessary when the software is localized for a particular experiment.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register name</span></th>
    <th class="nowrap"><span class="mgr-20">Address</span></th>
    <th class="nowrap"><span class="mgr-10">Operation</span></th>
    <th class="nowrap"><span class="mgr-10">Bit width</span></th>
    <th>Description</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>Register to select Trigger source for TRM.</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>ON/OFF DAQ gate. If DAQ gate is 0, TRM does not send trigger.</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Write access to this address asserts a soft reset to Event Builder, and the self event counter is reset to zero. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td>InitDDR</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Initialization to DDR receiver. (Don't care about the register value.)</td>
  </tr>
  <tr>
    <td>CtrlReg</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Register to control DDR receiver. Details are described later.</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">4</td>
    <td>Status register of DDR receiver. Details are described later.</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Sets signal to NIMOUT1.</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Sets signal to NIMOUT2.</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Sets signal to NIMOUT3.</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Sets signal to NIMOUT4.</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Selects NIMIN for extL1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Selects NIMIN for extL2.</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Selects NIMIN for Ext Clear</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Selects NIMIN for Ext Busy</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td class="tcenter">0x2090'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Selects NIMIN for coarse count reset of Mezzanine HR-TDC. Used to synchlonize multiple HR-TDCs. </td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Bus Bridge Primary: BBP (module ID = 0x3, 0x4)</b></td></tr>
  <tr>
    <td>Txd</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">32</td>
    <td>Data to be written to the slot-U secondary FPGA (FPGA on mezzanine card) via local bus bridge.</td>
  </tr>
  <tr>
    <td>Rxd</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Data read from the Slot-U secondary FPGA via the local bus bridge.</td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x3100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Assert the start signal to drive the Bus bridge primary and communicate with the slot-U secondary FPGA.</td>
  </tr>
  <tr>
  <td>Txd</td>
    <td class="tcenter">0x4000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">32</td>
    <td>Data to be written to the slot-D secondary FPGA (FPGA on mezzanine card) via local bus bridge.</td>
  </tr>
  <tr>
    <td>Rxd</td>
    <td class="tcenter">0x4010'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">32</td>
    <td>Data read from the Slot-D secondary FPGA via the local bus bridge.</td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x4100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>Assert the start signal to drive the Bus bridge primary and communicate with the slot-D secondary FPGA.</td>
  </tr>
  </tbody>
</table>

**Trigger Manager (TRM)**<br>
HUL HR-TDC BASE can not mount HRM; registers for HRM in TRM does not function.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register name</span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">Description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>RegL1Ext</td>
    <td>0x1</td>
    <td>Select NIMIN as L1 trigger source.</td>
  </tr>
  <tr>
    <td>RegL1J0</td>
    <td>0x2</td>
    <td>Select J0 bus as L1 trigger source</td>
  </tr>
  <tr>
    <td>RegL2Ext</td>
    <td>0x8</td>
    <td>Select NIMIN as L2 trigger source.</td>
  </tr>
  <tr>
    <td>RegL2J0</td>
    <td>0x10</td>
    <td>Select J0 bus as L2 trigger source</td>
  </tr>
  <tr>
    <td>RegClrExt</td>
    <td>0x40</td>
    <td>Select NIMIN as Clear source</td>
  </tr>
  <tr>
    <td>RegClrJ0</td>
    <td>0x80</td>
    <td>Select J0 bus as Clear source</td>
  </tr>
  <tr>
    <td>RegEnL2</td>
    <td>0x200</td>
    <td>0: L2=L1 trigger、1: L2=L2入力</td>
  </tr>
  <tr>
    <td>RegEnJ0</td>
    <td>0x400</td>
    <td>Use Tag info from J0 bus. Sends module busy to J0 bus if this bit is 1.</td>
  </tr>
</tbody>
</table>

**DAQ controller (DCT)**

Details of `CtrlReg` and `Status` bits in DCT.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register label</span></th>
    <th class="nowrap"><span class="mgr-30">Bit number</span></th>
    <th class="nowrap"><span class="mgr-10">Description</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>CTRL Registers</b></td></tr>
  <tr>
    <td>RegTestModeU</td>
    <td>1st bit (0x1)</td>
    <td>If this bit is ON, test pattern receiving mode for initialization of DDR receiver (slot-U).</td>
  </tr>
  <tr>
    <td>RegTestModeD</td>
    <td>2nd bit (0x2)</td>
    <td>If this bit is ON, test pattern receiving mode for initialization of DDR receiver (slot-D).</td>
  </tr>
  <tr>
    <td>EnableU</td>
    <td>3rd bit (0x4)</td>
    <td>If this bit is ON, DDR receiver (slot-U) becomes available. </td>
  </tr>
  <tr>
    <td>EnableD</td>
    <td>4th bit (0x8)</td>
    <td>If this bit is ON, DDR receiver (slot-D) becomes available.</td>
  </tr>
  <tr>
    <td>FRstU</td>
    <td>5th bit (0x10)</td>
    <td>When this bit is set, it asserts a force reset signal to the FPGA on slot-U. A function implemented in the MIF block in firmware v3.7 and earlier.</td>
  </tr>
  <tr>
    <td>FRstD</td>
    <td>6th bit (0x20)</td>
    <td>When this bit is set, it asserts a force reset signal to the FPGA on slot-D. A function implemented in the MIF block in firmware v3.7 and earlier.</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>Status Registers</b></td></tr>
  <tr>
    <td>BitAlignedU</td>
    <td>1st bit (0x1)</td>
    <td>Data readout has become ready after finishing the bit slip in DDR receiver (slot-U).</td>
  </tr>
  <tr>
    <td>BitAlignedD</td>
    <td>2nd bit (0x2)</td>
    <td>Data readout has become ready after finishing the bit slip in DDR receiver (slot-D).</td>
  </tr>
  <tr>
    <td>BitErrorU</td>
    <td>3rd bit (0x4)</td>
    <td>Initialization failed; bit slip was tried for designated times on DDR receiver (slot-U), but not replied correctly. </td>
  </tr>
  <tr>
    <td>BitErrorD</td>
    <td>4th bit (0x8)</td>
    <td>Initialization failed; bit slip was tried for designated times on DDR receiver (slot-D), but not replied correctly. </td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

HRM is not supported and some of the registers do not function.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Register label </span></th>
    <th class="nowrap"><span class="mgr-20">Register value</span></th>
    <th class="nowrap"><span class="mgr-10">Description</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>Signals available for NIMOUT</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busy, not including J0 bus busy nor ExtBusy.</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DAQ gate of DCT.</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 #8</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHz clock</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutU</td>
    <td class="tcenter">0xD</td>
    <td>Assign the trigger output from mezzanine HR-TDC in Slot-U.</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutD</td>
    <td class="tcenter">0xE</td>
    <td>Assign the trigger output from mezzanine HR-TDC in Slot-D.</td>
  </tr>
  <tr>
    <td>Reg_o_TrigOutUD</td>
    <td class="tcenter">0xF</td>
    <td>Assigns the logical OR of the trigger output from both mezzanine HR-TDCs in Slot-U/D.</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>NIMIN available for signals</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>Assign to NIMIN1</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>Assign to NIMIN2</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>Assign to NIMIN3</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>Assign to NIMIN4</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>If selected, Default setting are assgined to NIM IN/OUTs.</td>
  </tr>
</tbody>
</table>

IOM Default registers.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM OUT</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_DaqGate</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_clk1kHz</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_DIP8</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Signal</span></th>
    <th class="nowrap"><span class="mgr-20">Register</span></th>
    <th class="nowrap"><span class="mgr-10">Default</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
</tbody>
</table>

**DIP SW2 functions**<br>
Lists functions assigned to DIP SW2.

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">Switch number</span></th>
    <th class="nowrap"><span class="mgr-30">Function</span></th>
    <th class="nowrap"><span class="mgr-10">Detail</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ON for SiTCP forced default mode. Must be set before the power is turned on.</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Not in use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Force BUSY</td>
    <td>Crate Busy and Module Busy turned to high. For connection check.</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Bus BUSY</td>
    <td>ON to include J0 bus busy in Crate Busy, OFF to otherwise.</td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">LED</td>
    <td>ON to light LED4.</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Level</td>
    <td>Reflects DIP8 on IOM</td>
  </tr>
</tbody>
</table>

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED number</span></th>
    <th class="nowrap"><span class="mgr-10">Description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>Light for TCP open.</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>Light when module busy is high.</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>Light if DAQ gate is ON.</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>Light if DIP SW2 #5 (LED) is ON.</td>
  </tr>
</tbody>
</table>

### DAQ operation

Data flow is shown in [Figure](#DAQHRTDC). There are two FPGAs involved from Mezzanine HR-TDC and BASE, however, the DAQ operation is the same with that in  HUL MH-TDC. In each mezzanine, a partial event (up to the block buffer) is built and  transferred to the BASE, where the received data is collected to built  an event.

![daq-hrtdc-fw](daq-hrtdc-fw.png "Data flow block diagram of Mezzanine HR-TDC and HUL HR-TDC BASE"){: #DAQHRTDC width=90% }

**Module Busy timing**<br>
Module busy is the OR of the followings:

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">BUSY type</span></th>
    <th class="nowrap"><span class="mgr-30">BUSY length</span></th>
    <th class="nowrap"><span class="mgr-10">Description</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>Asserted since the L1 trigger.</td>
  </tr>
  <tr>
    <td>Mezzanine busy</td>
    <td class="tcenter">Depends on the Mezzanine status</td>
    <td>Busy from Mezzanine HR-TDC. Normally, the search window length of time. </td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>BUSY when the block buffer of TDC Base becomes Full. The busy means that TCP transfer cannot catch up, and is practically equivalent to SiTCP full.</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>TCP buffer of SiTCP becomes Full. It is asserted when the amount of data that the Event Builder is trying to send is large for the network bandwidth.</td>
  </tr>
</tbody>
</table>

**Data structure**<br>**Header word**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF80EB                                         |

Header2 (event size)
|      0xFF00      |  OverFlow  |    "000"    |         Number of word (12-bit)                |
```
Number of word indicates the number of words contained in the data body. It includes the two words for Sub-header. So the lowest value is 2. OverFlow stands when there is an over flow channel even for 1ch in the entire HUL.
```
Header3 (event number)
|   0xFF   | "0000" |  Tag (4-bit)  |                 Self counter (16-bit)                    |
```
Tag is the 4-bit Tag information  from TRM. The lower 3 bits are the lower 3 bits of the RM Event Number, and the 4th bit is the least significant bit of the RM spill number. Self-counter is a local event number that is incremented each time an event is forwarded. Starts with 0.

```
Sub-headers
|  0xFA00  | "0" |  OverFlow  |  StopDout  |  Through  |           # of word (12-bit)          |
|  0xFB00  | "0" |  OverFlow  |  StopDout  |  Through  |           # of word (12-bit)          |
```
Header of mezzanine HR-TDC. `0xFA00` and` 0xFB00` correspond to slot -U and -D, respectively. Overflow indicates the presence of over flow in each mezzanine. StopDout and Through indicate the state of `Stop Dout` and` Through` in `HRTDC_MZN::TDC::Controll`, respectively. "Number of word" indicates the number of words for each mezzanine.
<br><br>
**Data body**

```
Data body
|  Magic word (3-bit)  |  Ch (5-bit)  |                     TDC (24-bit)                       |
```

The *Magic word* is defined as follows.
<ul>
  <li> 6 Leading
  <li> 5 Trailing
  <li> 4 Common stop
</ul>

*Ch* can only be counted up to 31ch as it is 5 bit width. Check if the data belongs to subheaders A or B, decode it, and if it belongs to subheader B, add 32 channels. As mentioned in the section for HR-TDC, *TDC* is estimator (11bit) + semi-coarse count (2bit) + coarse count (11bit), which makes a total of 24 bits. When *Through* is ON, the fine-count appears at the estimator position.

## Three-dimensional matrix trigger

このファームウェアのバージョン表記は他のFWと異なっており、メジャーバージョンが1ですがFMPやSDSは搭載されています。

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0xe033</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x01</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x01</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v1.1</td>
    <td class="tcenter">2020.12.02</td>
    <td>実験で利用した最終版</td>
  </tr>
  </tbody>
</table>

**動作概要**<br>
このファームウェアはJ-PARC E03実験とE42実験の際に用いられたマトリックスコインシデンストリガーです。似たような機能を持つマトリックストリガー回路を作成したい場合の例題として利用してください。本ファームウェアにはデータ収集機能はありません。このFWでは3種類のホドスコープの三次元相関からトリガーを生成します。[図](#MTXFW)にブロック図を示します。この実験ではBH2、TOF、SCHという3種類のタイミングホドスコープ間のマトリックス相関を用います。丸カッコ内の数字はチャンネル番号を表しており、合計14336パターン (8x28x64) の組み合わせが発生します。入力は二重FFで同期されます。クロック速度は350 MHzです。後述のDWGやマトリックスパターンのブロックも同様のクロックで駆動されています。BH2がNIM-INへも繋がれているのはテストを簡便に行うためです。このファームウェアはメザニンスロットがSCHの入力を受けるためDCR v1/v2が必須です。

同期された入力信号はDelay Width Generator (DWG) で幅と遅延時間の調整がされます。各DWGはRBCPを通じて調整が可能です。350 MHz (2.857... ns) のクロック精度で32段階の調整が可能です。詳しくはソフトウェアの項で述べます。DWGではパルス出力中にもう一度パルス入力があった場合2つのパルスを繋げます。麻痺型モデルで表されるデッドタイムの振る舞いと同様です。

MTX3DとMTX2Dはそれぞれ三次元マトリックスコインシデンスと二次元マトリックスコインシデンスのブロックです。このFWでは1つ三次元トリガーと2つの二次元トリガーが実装されており、それぞれマトリックスパターンを設定可能です。RBCPを用いて全てのマトリックスエレメント1つ1つのOn/Offの切り替えが可能です。どのように実現しているかは後述します。

各マトリックストリガーはIOMを通じてNIMOUTから出力可能です。このFWでは実験の要求から二次元マトリックスの出力を三次元マトリックスの結果でVETOする経路が用意されています。この実験では三次元トリガーがビームVETOの役割を果たしていたためタイミングが良く分かっており、二次元トリガーに対しては固定長ディレイでVETO位置を調整しています。このあたりは実験条件に合わせて変更してください。


![mtx3d-fw](mtx3d-fw.png "Matrix3D triggerのブロック図。左側入力、右側が出力。"){: #MTXFW width=90% }

### マトリックスパターンの設定方法

14336パターンのスイッチをアドレスで解決しようと思うととてつもなく大きなエンコーダが必要になり、一般的にFPGAで実現するのは非現実的です。ここで3次元のレジスタを(28x64)x8と分解すると、8-bit幅・1792長のレジスタと捉えることが出来ます。すなわち、8-bit幅で長さが1792のシフトレジスタを用意することで、全てのレジスタビットの設定が可能となります。二次元の場合1-bit幅で長さが1792のシフトレジスタを用意します。RBCP (BCT) は同一のアドレスに1792回書き込むだけで設定が実現でき、極めてリソース効率が良いです。レジスタ設定はシステムクロックに対して低速なクロックで行っても良いため、このFWでは10 MHzのクロックでシフトレジスタを駆動しています。

### DWGの構造

DWGは遅延と幅の生成をシフトレジスタへのビットパターンのプリセットによって実現しています。例えば先頭から`00011111000...`というビットパターンを入力があったタイミングでシフトレジスタへセットしたとします。そうするとこのパターンは遅延量が3でパルス幅が5の波形を与えます。遅延量と幅の情報からビットパターンへの変換はソフトで行う事にしています。最大幅と遅延量がそれぞれ32のため、DWGの取るレジスタ幅は64-bitです。

### レジスタマップ

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX2D-1 (module ID = 0x00)</b></td></tr>
  <tr>
    <td>TofDWG</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (1-24) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TofExtDWG</td>
    <td class="tcenter">0x0010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (25-28) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>SchDWG</td>
    <td class="tcenter">0x0020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TrigDWG</td>
    <td class="tcenter">0x0030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>二次元トリガー出力のDWGを設定します。</td>
  </tr>
  <tr>
    <td>EnableMtx</td>
    <td class="tcenter">0x0040'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">1</td>
    <td>二次元マトリックスパターンを設定するアドレスです。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>ExecSR</td>
    <td class="tcenter">0x0100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>二次元マトリックスパターンを設定するシフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX2D-2 (module ID = 0x01)</b></td></tr>
  <tr><td class="tcenter" colspan=5>各レジスタはMTX2D-1と同様です</td></tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: MTX3D (module ID = 0x02)</b></td></tr>
  <tr>
    <td>TofDWG</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (1-24) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TofExtDWG</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>Tof (25-28) 入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>SchDWG</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>Bh2DWG</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>SCH入力のDWGレジスタを設定します。</td>
  </tr>
  <tr>
    <td>TrigDWG</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">64</td>
    <td>三次元トリガー出力のDWGを設定します。</td>
  </tr>
  <tr>
    <td>EnableMtx</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">8</td>
    <td>三次元マトリックスパターンを設定するアドレスです。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>ExecSR</td>
    <td class="tcenter">0x2100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>三次元マトリックスパターンを設定するシフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: IOM (module ID = 0x03)</b></td></tr>
  <tr>
    <td>Nimout1</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout1ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout2</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout2ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout3</td>
    <td class="tcenter">0x3020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout3ポートへの出力を設定します。</td>
  </tr>
  <tr>
    <td>Nimout4</td>
    <td class="tcenter">0x3030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>Nimout4ポートへの出力を設定します。</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

<table class="vmgr-table">
  <thead><tr>  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_Trig3D</td>
    <td class="tcenter">0x0</td>
    <td>MTX3Dトリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2DVeto1</td>
    <td class="tcenter">0x1</td>
    <td>MTX3DでVETOされた後のMTX2D-1トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2DVeto2</td>
    <td class="tcenter">0x2</td>
    <td>MTX3DでVETOされた後のMTX2D-2トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2D1</td>
    <td class="tcenter">0x3</td>
    <td>MTX2D-1トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Trig2D2</td>
    <td class="tcenter">0x4</td>
    <td>MTX2D-2トリガーを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_TofOR</td>
    <td class="tcenter">0x5</td>
    <td>TofORを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_SchOR</td>
    <td class="tcenter">0x6</td>
    <td>SchORを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Bh2OR</td>
    <td class="tcenter">0x7</td>
    <td>Bh2ORを出力します。</td>
  </tr>
  </tbody>
</table>

## Mass trigger (TOF based trigger)

**このファームウェアは2023年現在更新がストップしています。Mezzanine HR-TDC v5.0や最新のソフトウェアとは互換性がありません。以前のバージョンの物を引き続き利用してください。**

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x20d1</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x00</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v3.0</td>
    <td class="tcenter">2020.12.02</td>
    <td>実験で利用した最終版</td>
  </tr>
  </tbody>
</table>

Mass trigger (MsT) はJ-PARC K1.8での通称であり、機能的にはTOFベースのトリガー生成回路です。ダイポール磁気スペクトロメータを通過した粒子の軌道は大雑把に粒子の運動量と相関があります。2つのホドスコープの二次元マトリックス相関から運動量範囲を制限し、各マトリックスエレメントに対してTOFの分布を取ると粒子の質量ごとにTOFピークが分離します（低い運動量であれば）。Mass triggerはmezzanine HR-TDCとHULのメイン入力を用いて、二次元マトリックスとTOF情報によるトリガー生成を行うためのファームウェアです。HR-TDCの情報を得るためにはcommon stop入力が必要であるため、mass triggerはlevel2 decisionを行い、level2 triggerかもしくはfast clear信号を生成することが仕事です。

ブロック図を[図](#MSTFW)に示します。MsTはHRMとHR-TDCのメザニンカードを必要としています。それぞれ、slot-Uとslot-Dへマウントしてください。Main INへ入力された信号は低速なTDCでデジタイズされヒットレジスタ情報を与えます。そのため、このファームウェアにおけるTOFは厳密には検出器間の時間差でなく、common stopとの時間差です。Mass triggerを利用するためにはlevel1 triggerが非同期回路で生成されている必要があります。この仕様はK1.8ビームラインの都合に合わせてあるため、真のTOFを計算させたい場合ファームウェアの改修が必要になります。

SCH (64ch)とTOF (32ch) 間の2048パターンに対してTOF windowのチェックを行います。この時、HR-TDCから返ってきたマルチヒットデータ全てに対して判定を行うため、同一チャンネルに対して複数回の判定が行われることがあります。1つでもアクセプト範囲にTOF値があればトリガーが発行されます。1つのアクセプト範囲にデータがない場合クリア信号が出力されます。アクセプト窓の設定はSiTCP経由で行います。MsTでトリガー判定を行いたいのはlevel1 triggerが物理トリガーの場合だけです。キャリブレーショントリガーの場合は判定無しでlevel2 triggerを発生させなければいけません。判定すべきlevel1 triggerかどうかを知らせるために、このファームウェアではpiK flagという信号をlevel1 triggerに続いて入力することになっています。Flag入力が無ければ判定を行わず、必ずlevel2 triggerが発行されます。

本ファームウェアにはデータ収集機能が存在します。HR-TDCとLR-TDCによって得られた時間情報と、MsTの判定結果がデータとして返ってきます。本ファームウェアはトリガー生成回路ですが、データ収集のためには外部からトリガー入力が必要です。

![mst-fw](mst-fw.png "Mass triggerのブロック図。"){: #MSTFW width=90% }

**タイミングチャート**<br>
K1.8ビームラインでmass triggerを導入する目的は、主にVMEモジュール用にfast clearを生成し不必要なバスアクセス時間を減らす事です。K1.8ビームラインではchained-block-transfer (CBLT) でVMEバスアクセスを行っていますが、おおよそ100 usのバスアクセス時間がかかります。VMEモジュール内のmulti-event bufferを活用して、バスアクセス時間をlevel1 triggerに対するbusyに含めない工夫はしていますが、潜在的なbusyであるためlevel1 triggerのレートが7 kHzあたりから急激にDAQ効率が悪くなります。そのような実験ではmass triggerを導入してlevel2判定を加えてバスアクセスの回数を減らします。

[図](#MSTCHART)に想定タイミングチャートを示します。Mass triggerはHR-TDCとLR-TDCにcommon stop入力が入った時点から動作を開始します。HR-TDCからデータの転送が終わると判定を開始します。Mass triggerはmulit-hitを全て処理するため、ここまでの時間は可変です。Level1から固定時間後に判定結果を出力するために`MST::TimerPreset`で出力までの時間を設定します。今のファームウェアバージョンでは500程度の大きな値を設定することを推奨しています。`MST::TimerPreset`は判定プロセスよりも優先度が高いため、指定時間後に判定が終わっていなかった場合後述のno decision flagを立てたうえで強制的にlevel2 triggerを出力します。No decision flagが頻繁に立つ場合`MST::TimerPreset`の値が小さすぎます。

![mst-timing](mst-timing.png "想定しているlevel1 triggerからlevel2判定までのタイムチャート。"){: #MSTTIMING width=90% }

**判定フロー**<br>
Level2 trigger判定のフロー図を[図](#MSTCHART)に示します。図の下側に書かれているリストは各判定状態におけるフラグ（および内部信号）の状況を示しています。判定動作はlevel1 trigger受信で開始します。TDCブロックから情報を集め判定回路が各チャンネル独立に動作します。`MST::TimerPreset`に指定した時間がたった後、piK flagを受信しているか、および判定プロセスがすべて終了しているかのチェックを行います。どちらかを満たしていない場合、no decision flagを立ててlevel2 triggerを出力します。次にHR-TDCから取得したTDC値がアクセプト範囲にあるかどうかのチェックを行います。1つでも存在すればmass trigger acceptととなり、level2 triggerが出力されます。1つもない場合clear判定となり、敗者復活判定へ続きます。Clear判定を下されたイベントはDAQで取得されないため、クリアしているイベントをサンプル検査するために`MST::ClearPreset`に指定した値に1度、敗者復活アクセプトを出します。敗者復活判定が下された場合consolation acceptのフラグが立ち、level2 triggerが出力されます。敗者復活の条件を満たさない場合fast clearが出力されます。

![mst-chart](mst-chart.png "Mass triggerの判定フロー。"){: #MSTCHART width=90% }

**TOFのアクセプト範囲の設定**<br>
Mass triggerには2種類のTDC範囲設定を行うレジスタが存在します。`LRTDC::WinMax`と`LRTDC::WinMin`、およびmezzanine HR-TDCのウィンドウレジスタはcommon stop入力に対してヒットサーチを行う範囲を設定します。これは通常のTDCファームウェアと同様です。`MST::WixMax`と`MST::WinMin`はlevel2判定でアクセプトとなる範囲を与えます。Mass triggerでは32 chのHR-TDC入力と64 chのLR-TDC入力間の二次元マトリックスに対して、行列要素毎にこのレンジを設定することが出来ます。マトリックスコインシデンストリガーの時と同様に24-bit幅で長さが2048のシフトレジスタを採用しています。`MST::WixMax`と`MST::WinMin`の両方を0に設定すると、その行列要素に対しては判定を行いません。

### レジスタマップ

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>Trigger Manager: TRM (module ID = 0x00)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>TRM内部のトリガーポートの選択を行うレジスタ。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x01)</b></td></tr>
  <tr>
    <td>DaqGate</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>DAQ gateのON/OFF。DAQ gateが0だとTRMはtriggerを出力できない。</td>
  </tr>
  <tr>
    <td>EvbReset</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>このアドレスへ書き込み要求することでEVBのソフトリセットがアサートされ、Event builder内部のセルフイベントカウンタが0になる。</td>
  </tr>
  <tr>
    <td>InitDDR</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>DDR receiverへ向けて初期化要求を行う。</td>
  </tr>
  <tr>
    <td>CtrlReg</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">4</td>
    <td>DDR receiverを制御するためのレジスタ。</td>
  </tr>
  <tr>
    <td>Status</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R</td>
    <td class="tcenter">4</td>
    <td>DDR receiverのステータスレジスタ。</td>
  </tr>
    <tr><td class="tcenter" colspan=5><b>IO Manager: IOM (module ID = 0x02)</b></td></tr>
  <tr>
    <td>NimOut1</td>
    <td class="tcenter">0x2000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT1へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut2</td>
    <td class="tcenter">0x2010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT2へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut3</td>
    <td class="tcenter">0x2020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT3へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>NimOut4</td>
    <td class="tcenter">0x2030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">5</td>
    <td>NIMOUT4へ何を出力するかを設定する。</td>
  </tr>
  <tr>
    <td>ExtL1</td>
    <td class="tcenter">0x2040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL1にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td class="tcenter">0x2050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>extL2にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtClr</td>
    <td class="tcenter">0x2060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext clearにどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>ExtBusy</td>
    <td class="tcenter">0x2070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Ext busy入力にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td class="tcenter">0x2090'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Mezzanine HR-TDC内部のcoarse countをリセットするハードリセット信号。複数台のHR-TDCを同期したい場合に使用する。この線にどのNIMINを接続するか設定。</td>
  </tr>
  <tr>
    <td>PiKTrig</td>
    <td class="tcenter">0x20A0'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">3</td>
    <td>Mass triggerに対する物理トリガーフラグ入力。Level1 triggerに続いて入力があるとlevel2判定を行う。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>IO Manager: MIF-Down</b></td></tr>
  <tr>
    <td>Connect</td>
    <td class="tcenter">0x3000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">-</td>
    <td>MIF-Downからmezzanine HR-TDCのBCTへ向けて通信プロセスを開始する。アクセスする際のモードが書き込みなのか読み出しなのかで、メザニンのBCTへのアクセス方法が切り替わる仕様となっている。</td>
  </tr>
  <tr>
    <td>Reg</td>
    <td class="tcenter">0x3010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">20</td>
    <td>MIF-Downにmezzanine HR-TDC用のlocal addressと書き込み用レジスタ値をMIFに一時的に保存する。<br>
        <ul>
          <li> [19:8]: Local address</li>
          <li> [7:0]: Register valus</li>
        </ul>
    </td>
  </tr>
  <tr>
    <td>ForceReset</td>
    <td class="tcenter">0x3100'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>MIF-Downのmezzanine HR-TDCへ強制リセット信号をアサート。DAQやBCTがハングした場合に使用する。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Low-resolution TDC: LRTDC</b></td></tr>
  <tr>
    <td>PtrOfs</td>
    <td class="tcenter">0x4010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>内部制御変数。ユーザーは触らない。</td>
  </tr>
  <tr>
    <td>WindowMax</td>
    <td class="tcenter">0x4020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の上限値。1-bitが6.666... nsに相当。</td>
  </tr>
  <tr>
    <td>WindowMin</td>
    <td class="tcenter">0x4030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">11</td>
    <td>Ring bufferからヒットを探す時間窓の下限値。1-bitが6.666... nsに相当。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Mass trigger: MST</b></td></tr>
  <tr>
    <td>ClearPreset</td>
    <td class="tcenter">0x5000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">7</td>
    <td>このレジスタに書かれた値に1度、敗者復活アクセプトが出力される。0に設定すると敗者復活判定を行わない。</td>
  </tr>
  <tr>
    <td>TimerPreset</td>
    <td class="tcenter">0x5010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">9</td>
    <td>Level1 triggerを受信してからlevel2 trigger/clearを出力するまでの遅延時間。</td>
  </tr>
  <tr>
    <td>WinMax</td>
    <td class="tcenter">0x5020'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">24</td>
    <td>アクセプトするTOF範囲の上限値を与えます。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>WinMin</td>
    <td class="tcenter">0x5030'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">24</td>
    <td>アクセプトするTOF範囲の下限値を与えます。シフトレジスタの最下位にレジスタを書きます。</td>
  </tr>
  <tr>
    <td>Exec</td>
    <td class="tcenter">0x5040'0000</td>
    <td class="tcenter">W</td>
    <td class="tcenter">-</td>
    <td>シフトレジスタを1つシフトさせます。</td>
  </tr>
  <tr>
    <td>Bypass</td>
    <td class="tcenter">0x5050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>このレジスタが1になると判定回路をバイパスしてlevel2 triggerを出力するようになります。実験中に一時的にmass triggerをバイパスしたい場合に利用します。</td>
  </tr>
  </tbody>
</table>

**I/O Manager (IOM)**

<table class="vmgr-table">
  <thead><tr>  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタラベル</span></th>
    <th class="nowrap"><span class="mgr-20">レジスタ値</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=3><b>NIMOUTへ出力可能な内部信号</b></td></tr>
  <tr>
    <td>Reg_o_ModuleBusy</td>
    <td class="tcenter">0x0</td>
    <td>Module busyです。Module busyは自身の内部busyのみを指します。J0 busのbusyやExtBusyは含まれません。</td>
  </tr>
  <tr>
    <td>Reg_o_CrateBusy</td>
    <td class="tcenter">0x1</td>
    <td>CrateBusyです。CrateBusyはmodule busyに加えてJ0 busのbusyやExtBusyを含みます。J0 busマスタの場合に利用する信号になり、またHRMが Trigger Moduleへ返すbusyと同等です。</td>
  </tr>
  <tr>
    <td>Reg_o_RML1</td>
    <td class="tcenter">0x2</td>
    <td>HRMが受信したL1 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RML2</td>
    <td class="tcenter">0x3</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMClr</td>
    <td class="tcenter">0x4</td>
    <td>HRMが受信したL2 triggerを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMRsv1</td>
    <td class="tcenter">0x5</td>
    <td>HRMが受信したReserve 1を出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_RMSnInc</td>
    <td class="tcenter">0x6</td>
    <td>HRMがSpill Number Incrementを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DaqGate</td>
    <td class="tcenter">0x7</td>
    <td>DCTのDAQ gateを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_DIP8</td>
    <td class="tcenter">0x8</td>
    <td>DIP SW2 8番のレベルを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1MHz</td>
    <td class="tcenter">0x9</td>
    <td>1 MHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk100kHz</td>
    <td class="tcenter">0xA</td>
    <td>100 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk10kHz</td>
    <td class="tcenter">0xB</td>
    <td>10 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_clk1kHz</td>
    <td class="tcenter">0xC</td>
    <td>1 kHzのクロックを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Accept</td>
    <td class="tcenter">0xD</td>
    <td>Accept flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Clear</td>
    <td class="tcenter">0xE</td>
    <td>Clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_ConsolationAccept</td>
    <td class="tcenter">0xF</td>
    <td>Consolation accept flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_FinalClear</td>
    <td class="tcenter">0x10</td>
    <td>Final clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_Level2</td>
    <td class="tcenter">0x11</td>
    <td>Level2 flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_FastClear</td>
    <td class="tcenter">0x12</td>
    <td>Fast clear flagを出力します。</td>
  </tr>
  <tr>
    <td>Reg_o_NoDecision</td>
    <td class="tcenter">0x13</td>
    <td>No decision flagを出力します。</td>
  </tr>
  <tr><td class="tcenter" colspan=3><b>内部信号線へ割り当て可能なNIMINポート</b></td></tr>
  <tr>
    <td>Reg_i_nimin1</td>
    <td class="tcenter">0x0</td>
    <td>NIMIN1番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin2</td>
    <td class="tcenter">0x1</td>
    <td>NIMIN2番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin3</td>
    <td class="tcenter">0x2</td>
    <td>NIMIN3番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_nimin4</td>
    <td class="tcenter">0x3</td>
    <td>NIMIN4番を信号線へアサインします。</td>
  </tr>
  <tr>
    <td>Reg_i_default</td>
    <td class="tcenter">0x7</td>
    <td>このレジスタが設定された場合、指定のデフォルト値がそれぞれの内部信号線へ代入されます。</td>
  </tr>
</tbody>
</table>

以下はIOMレジスタの初期値のテーブルです。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">NIM出力ポート</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10"></span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>NIMOUT1</td>
    <td>Reg_o_ModuleBusy</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT2</td>
    <td>reg_o_RML1</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT3</td>
    <td>reg_o_RML2</td>
    <td></td>
  </tr>
  <tr>
    <td>NIMOUT4</td>
    <td>reg_o_RMClr</td>
    <td></td>
  </tr>
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">内部信号線</span></th>
    <th class="nowrap"><span class="mgr-20">初期レジスタ</span></th>
    <th class="nowrap"><span class="mgr-10">デフォルト値</span></th>
  </tr></thead>
  <tr>
    <td>ExtL1</td>
    <td>Reg_i_Nimin1</td>
    <td>NIMIN1</td>
  </tr>
  <tr>
    <td>ExtL2</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLClear</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>ExtLBusy</td>
    <td>Reg_i_nimin3</td>
    <td>NIMIN3</td>
  </tr>
  <tr>
    <td>ExtLRsv2</td>
    <td>Reg_i_nimin4</td>
    <td>NIMIN4</td>
  </tr>
  <tr>
    <td>cntRst</td>
    <td>Reg_i_default</td>
    <td>0</td>
  </tr>
  <tr>
    <td>PiKFlag</td>
    <td>Reg_i_nimin2</td>
    <td>NIMIN2</td>
  </tr>
</tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**<br>
HUL RMと同様です。

**LED点灯の機能**<br>
HUL RMと同様です。

### DAQの動作

Mass triggerからはLR-TDCとHR-TDCの測定結果、およびlevel2判定の結果（フラグ）が返ってきます。Level2判定はイベント毎に行うため、level2判定を行っている間BUSYになります。

**Module Busyとなるタイミング**<br>
BUSYの定義は以下に列挙するBUSY信号のORになります。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">BUSY種別</span></th>
    <th class="nowrap"><span class="mgr-20">BUSY長</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td>Self busy</td>
    <td class="tcenter">210 ns</td>
    <td>L1 triggerを検出した瞬間から固定長でアサート。</td>
  </tr>
  <tr>
    <td>Sequence busy</td>
    <td class="tcenter">サーチ窓幅に依存</td>
    <td>Ring bufferからヒット情報を探している間、すなわち`WindowMax`-`WindowMin`分のBUSYが出力されます。Mass triggerにはLR-TDCとHR-TDCが存在するため、長いほうが採用されます。</td>
  </tr>
  <tr>
    <td>Block full</td>
    <td class="tcenter"> - </td>
    <td>ブロックバッファがfullになった段階でBUSYが出力されます。L1 triggerレートが後段の回路のデータ処理速度を上回るとアサートされます。つまりTCP転送が追いつかない事を意味するので、実質的にSiTCP fullと同等です。</td>
  </tr>
  <tr>
    <td>SiTCP full</td>
    <td class="tcenter"> - </td>
    <td>SiTCPのTCPバッファがFullになると出力されます。ネットワーク帯域に対してEvent Builderが送信しようとするデータ量が多いとアサートされます。</td>
  </tr>
  <tr>
    <td>Decision busy</td>
    <td class="tcenter"> `MST::TimerPreset`に依存</td>
    <td>Level1 trigger受信からlevel2/clear出力までの間BUSYになります。`MST::TimerPreset`に依存します。</td>
  </tr>
</tbody>
</table>

**データ構造**<br>**ヘッダワード**
```
Header1 (Magic word)
MSB                                                                                          LSB
|                                           0xFFFF20D1                                         |

Header2 (event size)
|   0xFF   | "00" |  OerFlow  |    "000"    |         Number of word (12-bit)                  |
```
Number of wordはデータボディに含まれるワード数を示します。Number of WordはSub-header分の2ワード分を含みます。なので最低値が2です。Over flowはHUL全体で1chでもover flowチャンネルがあると立ちます。
```
Header3 (event number)
|   0xFF   | HRM bit | "000" |  Tag (4-bit)  |             Self counter (16-bit)               |
```
TagはTRMから出力される4bitのTag情報です。下位3bitがRM Event Numberの下位3ビット、4ビット目がRM spill numberの最下位ビットとなります。Self-counterはイベント転送を行うたびにインクリメントされるlocal event numberで、0オリジンです。

```
Data body
RVM data
|  0xF9  | "00" |  Lock  |  SNI  |  Spill Num (8-bit))  |           Event Num (12-bit)         |

Mass trigger data
|                 0x2000                    |   "0000'0000'0"   |   Mass trigger flag (7-bit)  |

HR-TDC block
Sub-hearder
|  0x8000  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
HR-TDC data
|  Magic word (3-bit)  |  Ch (5-bit)  |                     TDC (24-bit)                       |

LR-TDC block
Sub-hearders
|  0xFC10  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
|  0xFC20  | "00" |  OerFlow  |  StopDuout  |  Through  |           # of word (11-bit)         |
LR-TDC data
|   0xCC   |  "0"  |   Ch (7-bit)   |   "00000"   |                 TDC (11-bit)               |
```
データボディ領域ではsub-headerに続いてその領域のデータが返ってきます。

Mass trigger flagの内訳
<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">ビット番号</span></th>
    <th class="nowrap"><span class="mgr-10">フラグ</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">0</td>
    <td class="tcenter">No decision</td>
  </tr>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">Level2</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Fast clear</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">Consolation accept</td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">Final clear</td>
  </tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Accept</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Clear</td>
  </tr>
  </tbody>
</table>

## Streaming TDC

<table class="vmgr-table">
  <tbody>
  <tr>
    <td>固有名</td>
    <td class="tcenter">0x11dc</td>
  </tr>
  <tr>
    <td>メジャーバージョン</td>
    <td class="tcenter">0x03</td>
  </tr>
  <tr>
    <td>マイナーバージョン</td>
    <td class="tcenter">0x05</td>
  </tr>
  </tbody>
</table>

**更新歴**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">バージョン</span></th>
    <th class="nowrap"><span class="mgr-10">リリース日</span></th>
    <th>変更点</th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">v3.5</td>
    <td class="tcenter">2021.4.</td>
    <td></td>
  </tr>
  </tbody>
</table>

**動作概要**<br>
このファームウェアはtrigger-less DAQ用に開発された外部トリガー無しに連続的に時間測定を行うTDCです。J-PARC MRの遅い取り出しの2秒間の間、1 nsの精度で時間測定を行うことを想定しています。Trigger-less DAQではフロントエンド回路上でトリガーによるイベント選別を行わないため、入力信号全てをデジタイズしPCへデータ転送を続けます。

HULはtrigger-less DAQにおいてdata streamingを行うにはデータリンクスピードが不十分であり、また十分なメモリ搭載していません。このファームウェアは連続時間測定の技術実証のために開発した側面が強いです。今後HULではこのファームウェアの開発は継続せず、**AMANEQ**というtrigger-less DAQ用に開発された回路に引き継ぐ予定です。Trigger-less DAQや連続読み出しに興味のある方は本多へ連絡をお願いいたします。

このような背景があるので、このファームウェアには試験的に実装した機能も存在します。ここでは汎用的に使えそうな部分のみ説明することにします。

**ブロック構造**<br>
入力はmain-inの上側を利用します。そのほか、テスト入力、VETO入力、スピルゲート入力をNIMINポートから行います。本TDCのチャンネル数は32 chです。テスト入力が有効の場合、main in Uの入力の代わりにNIMIN2からの信号が全チャンネルに配られます。高繰り返しのテスト信号を入れるとデータレートがすぐにリンクスピードを上回るので、利用時にはレートに気を付けてください。VETO入力はHIGHの間入力信号をマスクします。スピルゲートよりもさらに細かく入力信号の選択をしたい場合に利用してください。スピルゲートはJ-PARCの遅い取り出しのスピルゲートを想定しています。スピルゲートがHIGHの状態の時だけ、本TDCはデータを送信します。本TDCは端的にはスピルスタートからの時間を連続的に測るTDCです。そのため、スピルゲートは2秒間ONで2秒間OFFのように周期的にやってくることが前提になっています。

時間計測Online Data Processing (ODP) blockで行われます。ここで入力信号の立ち上がりと立下りの両エッジの時間を測定し、エッジペアを見つけます。この段階でTime-Over-Thresholdが計算され、立下りの時刻情報は破棄されます。以後、データワード内には立ち上がり時刻とTOT値が含まれます。

ODPブロックまでは各チャンネル毎に独立にデータ処理されます。Vital blockではデータパスを1つにまとめ上げ、データ列へハートビートデータという特殊データの挿入を行います。本TDCのコースカウントは125 MHzのクロックで駆動されている16-bitカウンタ (heartbeat generator)によって付与され、500 μs程度で1周します。それ以上の長さで時刻再構成を行うために、heartbeat methodという方式を導入しています。Heartbeat generatorが1周するごとにデータ列にheartbeatデータを挿入します。解析ではheartbeatデータの数を数えることで、スピルスタートからの時間を再構成することが出来ます。Heartbeatデータはデータ列のちょうど区切りの位置に挿入されなければならないため、vital blockにはtime frameの切れ目を見つける工夫が施されています。

データレートがSiTCPのスピードを超えた場合の対処など、さらに詳しい情報は[参考文献](https://doi.org/10.1093/ptep/ptab128)を参照してください。(R.Honda et al., PTEP, 2021)

![block-strtdc-fw](block-strtdc-fw.png "Streaming TDCのブロック図。"){: #STRTDCFW width=90% }

<table class="vmgr-table">
  <tr><td class="tcenter" colspan=2><b>Streaming TDC仕様</b></td></tr>
  <tr>
    <td>TDC精度</td>
    <td class="tcenter">0.97... ns</td>
  </tr>
  <tr>
    <td>最長スピルゲート長</td>
    <td class="tcenter">33秒</td>
  </tr>
  <tr>
    <td>最小パルス幅</td>
    <td class="tcenter">~4 ns</td>
  </tr>
  <tr>
    <td>最大パルス幅</td>
    <td class="tcenter">150 ns</td>
  </tr>
  <tr>
    <td>ダブルヒット分解能</td>
    <td class="tcenter">~7 ns</td>
  </tr>
  </tbody>
</table>

### レジスタマップ

TOT filerとtime-walk correctorはこのUGの中では説明していません。もし利用する場合は参考文献を読んでください。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">レジスタ名</span></th>
    <th class="nowrap"><span class="mgr-20">アドレス</span></th>
    <th class="nowrap"><span class="mgr-10">読み書き</span></th>
    <th class="nowrap"><span class="mgr-10">ビット幅</span></th>
    <th>備考</th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=5><b>DAQ Controller: DCT (module ID = 0x0)</b></td></tr>
  <tr>
    <td>SelectTrigger</td>
    <td class="tcenter">0x0000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">12</td>
    <td>DAQデータ取得を開始するためのゲート。</td>
  </tr>
  <tr><td class="tcenter" colspan=5><b>Online Data Processing block: ODP (module ID = 0x1)</b></td></tr>
  <tr>
    <td>EnFilter</td>
    <td class="tcenter">0x1000'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>TOT filterの機能を有効にします。</td>
  </tr>
  <tr>
    <td>MinTh</td>
    <td class="tcenter">0x1010'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>TOT filterの下限閾値を設定します。この値よりTOTが小さいとフィルターされます。</td>
  </tr>
  <tr>
    <td>MaxTh</td>
    <td class="tcenter">0x1020'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>TOT filterの上限閾値を設定します。この値よりTOTが大きいとフィルターされます。</td>
  </tr>
  <tr>
    <td>EnZeroThrough</td>
    <td class="tcenter">0x1030'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">1</td>
    <td>TOT値が0だったヒット（立下りエッジが見つからなかったヒット）をフィルターしないようにします。</td>
  </tr>
  <tr>
    <td>TwCorr0</td>
    <td class="tcenter">0x1040'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域0の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr1</td>
    <td class="tcenter">0x1050'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域1の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr2</td>
    <td class="tcenter">0x1060'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域2の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr3</td>
    <td class="tcenter">0x1070'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域3の補正値を設定します。</td>
  </tr>
  <tr>
    <td>TwCorr4</td>
    <td class="tcenter">0x1080'0000</td>
    <td class="tcenter">R/W</td>
    <td class="tcenter">8</td>
    <td>Time-walk correctorの領域4の補正値を設定します。</td>
  </tr>
  </tbody>
</table>

### HUL上のスイッチ・LEDの機能

**DIP SW2の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-10">スイッチ番号</span></th>
    <th class="nowrap"><span class="mgr-30">機能</span></th>
    <th class="nowrap"><span class="mgr-10">詳細</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">1</td>
    <td class="tcenter">SiTCP force default</td>
    <td>ONでSiTCPのデフォルトモードで起動します。電源投入前に設定している必要があります。</td>
  </tr>
  <tr>
    <td class="tcenter">2</td>
    <td class="tcenter">Enable test in</td>
    <td>ONにするとNIMIN2が全チャンネルの入力に接続されます。</td>
  </tr>
  <tr>
    <td class="tcenter">3</td>
    <td class="tcenter">MCD mode bit-1</td>
    <td>MCDメザニンを搭載した際の動作を決めるビットです。通常はOFF (0)に設定します。<br>
    <ul>
      <li> 0b11: Master
      <li> 0b10: Repeater
      <li> 0b01: Slave
      <li> 0b00: Stand alone
    </ul>
    </td>
  </tr>
  <tr>
    <td class="tcenter">4</td>
    <td class="tcenter">MCD mode bit-2</td>
    <td>MCDメザニンを搭載した際の動作を決めるビットです。通常はOFF (0)に設定します。</tr>
  <tr>
    <td class="tcenter">5</td>
    <td class="tcenter">Enable signal copy</td>
    <td>このビットがONだと、下側のメザニンコネクタへ入力信号のコピーが出力されます。別の回路でも信号を使いたい時に利用します。DTLメザニンカードを下側のメザニンスロットへ取り付けてください。</td>
  </tr>
  <tr>
    <td class="tcenter">6</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">7</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
  <tr>
    <td class="tcenter">8</td>
    <td class="tcenter">Not in Use</td>
    <td></td>
  </tr>
</tbody>
</table>

**LED点灯の機能**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">LED番号</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr>
    <td class="tcenter">LED1</td>
    <td>点灯中はTCP接続が張られています。</td>
  </tr>
  <tr>
    <td class="tcenter">LED2</td>
    <td>機能していません。</td>
  </tr>
  <tr>
    <td class="tcenter">LED3</td>
    <td>点灯中はスピルゲート入力がHIGHであることを示します。</td>
  </tr>
  <tr>
    <td class="tcenter">LED4</td>
    <td>機能していません。</td>
  </tr>
</tbody>
</table>


**NIM-IOへの信号アサイン**<br>
Streaming TDCにはIOMが存在しないため、入出力のアサインを変える事はできません。

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-30">NIMIO</span></th>
    <th class="nowrap"><span class="mgr-10">備考</span></th>
  </tr></thead>
  <tbody>
  <tr><td class="tcenter" colspan=2><b>NIM-IN</b></td></tr>
  <tr>
    <td class="tcenter">NIN-IN1</td>
    <td>スピルゲート入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN2</td>
    <td>テスト入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN3</td>
    <td>VETO入力。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-IN4</td>
    <td>未使用</td>
  </tr>
<tr><td class="tcenter" colspan=2><b>NIM-OUT</b></td></tr>
  <tr>
    <td class="tcenter">NIM-OUT1</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT2</td>
    <td>未使用</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT3</td>
    <td>NIM-IN1に入力されたスピルゲートのコピー出力。クロック同期を取った後の信号のため立ち上がりタイミングは量子化されている。</td>
  </tr>
  <tr>
    <td class="tcenter">NIM-OUT4</td>
    <td>未使用</td>
  </tr>
</tbody>
</table>

### DAQの動作

Streaming TDCはtirgger-less DAQで使用することを想定しているため、データ取得のトリガーという概念は存在しません。
データ転送を行う条件が揃うとFPGA即座にネットワークにデータを送信しようと試みるため、PC側は先にデータ準備が出来ていないといけません。
FPGAがデータを出力する条件は次の3つが全て成立している事です。
1. TCP connectionが成立している。
2. DAQ gateがONになっている。
3. Spill gateがON (NIMIN1の信号がHIGHである)。
本FWを使用する際には、上記順番の通りに処理することをお勧めします。
DAQ gateがONでspill gateがONになるとODPブロックはデータ計測を開始します。
この時点でTCP接続が確立していないと内部バッファにデータが溜まり続けていくため、バッファがあふれてしまう可能性があります。
TCP接続を確立してからRBCPでDAQ gateをONにしてください。

**データ構造**<br>
<span class=myred>Streaming TDCの1ワードは40-bitです。</span>受け取ったデータをそのまま`int32_t`や`int64_t`にキャストできないので、ソフトウェアの記述は工夫してください。
本FWにはspill start, TDC data, heartbeat, busy, spill endの5種類のデータが存在します。
それぞれ先頭の4ビットで識別が可能です。
Heartbeatデータはheartbeat (time) frameの境目に挿入され、spill startから何回目のheartbeatであるかを示すカウンターが下位16-bitに埋め込まれています。
もしvital blockの状態がbusyモードの場合にはheartbeatデータの代わりにbusyデータが返ってきます。

```
Spill start word
MSB                                                                                          LSB
|      0x1      |           RSV (20-bit)              |                  0xFFFF                |

TDC data
|      0xD      | '0' | TOT(8-bit) | Type(2-bit) | Ch(6-bit) |            TDC(19-bit)          |

Heartbeat data
|      0xF      |           RSV (20-bit)              |         heartbeat number (16-bit)      |
Busy data
|      0xE      |           RSV (20-bit)              |         heartbeat number (16-bit)      |

Spill end data
|      0x4      |           RSV (20-bit)              |                  0xFFFF                |
```

TDC dataに含まれるtypeは現状きちんと機能していません。
通常のTDC dataであれば`00`です。そのほかに`01`と`10`のパターンがありますが、これらであった場合そのデータは無視してください。

**Busyモード**<br>
FPGAが送信しようとするデータ量がデータリンクのスピードを超えると転送が追い付かなくなり、FPGA内部のバッファが溢れます。
このような状況にFWが陥ると、vital blockは通常の動作モードからbusyモードへ状態を遷移させ復帰を試みます。
Streaming TDCではtriggerを止めるためのBUSY信号は存在しませんが、busyモードではbusyデータがheatbeatデータの代わりに現れるようになります。
Busyデータが返ってきたheartbeatフレームではデータ欠落が起きていますが、どのデータをどれだけ落としたかは分かりません。
一部または全てのデータが欠落しているということだけが分かります。
一旦busyモードに移行すると最低3フレーム復帰するために必要とします。
更に詳しい動作については[参考文献](https://doi.org/10.1093/ptep/ptab128)を参照してください。

**データの並び**<br>
Streaming TDCから出力されるデータの並びを時間軸上に表した物を[図](#STRTDCDATA)に示します。
Spill gateの立ち上がりを検出するとまずspill start dataが出力されます。
次に最初のheatbeat frame内のTDC dataが続き、frameの境目にheartbeat (busy) dataが挿入されます。
Busy dataが返ってきた場合該当frameではデータ欠落が起きています。

![data-order-strtdc](data-order-strtdc.png "Streaming TDCから出力されるデータの並び順"){: #STRTDCDATA width=90% }
