# SiTCP

SiTCP is a hardware implementation of TCP/IP network communication protocol, developed by Tomohisa Uchida at KEK. It enables the communication without an involvement of a CPU. Please refer to Uchida's and BBT's web sites for detailed usage and the most recent IP core (the component to be linked when developing a firmware).
* [Uchida's web site](https://www.sitcp.net/)
* [BBT's web site](https://www.bbtech.co.jp/products/sitcp-library/)


SiTCP block diagram is shown in a  [figure below](#SITCPBLOCK). SiTCP initializes the ethernet communication PHY and loads MAC and IP addresses from EEPROM. In the forced default mode, the MAC and IP addresses are set to the default  (192.168.10.16). The forced default mode does not allow other SiTCP hardware in the same forced default mode to exist in the same network, usually used in the 1 to 1 connection to the PC for testing. SiTCP provides TCP and UDP communication protocols to FPGA. SiTCP supports both the 100 Mbps and 1Gbps modes automatically switched by the signal from PHY; however, the existing firmware for HUL only supports the 1Gbps mode. TCP generally transmits or receives data in the 8 bit units synchlonized with the system clock; however, the TCP receive action (data from PC to hardware in TCP) is not recommended in SiTCP. The registers on the hardware are accessed via UDP. A unique packet called RBCP is used in the UDP for control command transmission, address setting and data transmission and receiving. RBCP returns UDP acknowledge for handshaking between the PC and SiTCP. UDP transmits 32 bit address and transmits/receives data in 8 bits synchlonized with the system clock. SiTCP internal registers and the EEPROM are also addressed and may be accessed via UDP, however do not touch them unless really necessary.  The IP and MAC address are stored in EEPROM and loaded to SiTCP registers, which may be accessed by the BBT software (SiTCP tools) or specifying the UDP address in a user program.

SiTCP does not "keep alive" the connection by default. This causes a session closure in case of very low trigger rate (~1 trigger / 30 min). To avoide such situation, regularly send a keep alive packet.

![SiTCP](sitcp.png "SiTCP block diagram"){: #SITCPBLOCK width="50%" }

## MAC and IP address setting

MAC and IP addresses are stored in EEPROM and designed to be set via SiTCP. A firmware which has at least SiTCP must be loaded to FPGA to access the address. To set the address, a user program with RBCP communications may be written, however, SiTCP tools developed by BBT is also available from its web site after user registration.
To write the MAC address, use SiTCPMpcWriter in the BBT's tools. Select the file in the DVD purchased from GND Ltd (recommended) after starting the tool. MAC address has to be unique to each hardware; having the same MAC address in the same network causes a communication trouble. For more about SiTCPMpcWriter, refer to [BBT's web page](https://www.bbtech.co.jp/download-files/sitcp/index.html).

To write IP address, use SiTCP Utility (MS Window software) also available in multi platforms (beta-release). This procedure must be performed after installing the MAC address. Check on "access to EEPROM" which is marked in the red square in the [figure below](#SITCPUTIL). Specify the current IP address and press "show(表示)" button. If the "eye" in the upper right corner blinks and the information is shown, the access was successful. If the current IP address is unknown, start the hardware in the foced default mode (192.168.10.16).  Fill in the new IP address to set and press "rewrite(書き替え)". This loads the new IP address only to EEPROM, and the current IP address stays the same. To confirm the IP address  in EEPROM, again press the "show(表示)". This address is effective after turning on the harware power next time

![SiTCPUtil](sitcp-util.png "SiTCP Utility"){: #SITCPUTIL width="50%" }
