# Software

This chapter describes Linux software for control.

**Environment for Development**<br>
<ul>
  <li> CentOS7
  <li> gcc version 4.8.5
</ul>
The source code for HR-TDC depends on CERN ROOT, because it creates a ROOT file.
In an environment where ROOT cannot be installed, exclude HR-TDC from the make target.

**Direcotry structure**<br>
"CoreLib" provides the common library used by this software package. "Common" provides programs to control common elements in each firmware, such as SDS and FMP control. In addition, the source code dedicated to each firmware exists in the directory with the FW name.
```
hul_software.git
├── Makefile
├── CoreLib
├── Common
└── FW directories
```

## Source files

The source files may be categolized as
<ul>
<li> library-like files that should be copied as they are when porting to other data acquision (DAQ) systems,
<li>files that can be written in any way as long as they work, and <li>files that are for debugging and do not need to be ported.
</ul>Only the first category will be explained.  <br>
All executables are designed to display usage when started with no arguments.

**CoreLib**

rbcp.hh
  : The structure of the RBCP packet. Do not change.

Uncopyable.hh
  : A method that prohibits copying constructors and assignment operators in user-defined classes. This implementation is old, and the standard features of c ++ nowadays may do the same; this will be eliminated in the future.

BitDump
  : This class returns a bit string to the standard output when an integer value is given. It was originally used in Unpacker, and used in UDP RBCP. Useful for debugging.

Utility
  : Shows a progress bar or blinks text on the console. Used in the Flash Memory Programmer.

UDPRBCP
: It is the class for UDP communication of SiTCP and is an important source code. The constructor requires an IP address, upd port, and the last argument is display mode, defined in `UDPRBCP.hh` as an enumeration called ` RbcpDebugMode`.
<ul>
<li>`NoDisp` displays nothing during UDP communication.
<li> `Interactive` displays some information about the operation. <li>` Debug` displays all information available.
</ul>
For usual use, `NoDisp` will be fine. UDPRBCP provides the low-level UDP communication in SiTCP. HUL may be controlled with this class, but primitively.

FPGAModule
  : This class provides BusController (BCT) level communication function. The constructor takes a UDPRBCP as an argument to include UDPRBCP. Its main function is to support multi-byte reading and writing through BCT.
```c++
int32_t WriteModule(const uint32_t local_address,
                    const uint32_t write_data,
                    const int32_t  n_cycle = 1);
```
A method to write a register. The first argument is the RBCP address. See the register map in Chapter 4 and `RegisterMap.hh` described below. The second argument is the register value to write. The third argument is the number of bytes be written. The return value is the result of `UDCPRBCP::DoRBCP`. This function is overloaded for 32-bit and 64-bit registers. The range in which `n_cycle` can be taken is 1-4 (up to 4 bytes=32 bits) and 1-8 (up to 8 bytes = 64 bits), respectively.

```c++
uint32_t ReadModule(const uint32_t local_address,
                    const int32_t  n_cycle = 1);
```
A method to read a register. The first argument is the RBCP address. The second argument sets the number of multibyte to read. The read result appears in the return value of the function. Independent to n_cycle value, the return is in the type of `uint32_t`. This function is also overloaded for 32-bit and 64-bit.

```c++
int32_t WriteModule_nByte(const uint32_t  local_address,
                          const uint32_t* write_data,
                          const int32_t   n_byte);
```
A function for writing multiple bytes to the same address. This function does not increment the address on every write. The assumed write destination is FIFO.

```c++
int32_t ReadModule_nByte(const uint32_t  local_address,
                         const int32_t   n_byte);
```
A function  for reading multiple bytes from the same address. This function does not increment the address on every read. The assumed read source is FIFO. The read data is stored in a class variable called `rd_data_`. Get the iterator with `GetDataIterator` to access the data.

SiTCPController
  : A function to directly access the reserved area of ​​SiTCP. EraseEEPROM is a function to erase all EEPROM. Functions for reading and writing to the EEPROM area are not developed because they are dangerous. Using the SiTCP Utility (Chapter 3)  is recommended.
```C++
void ResetSiTCP(const std::string ip)
```
A function to soft reset SiTCP.

```C++
void WriteSiTCP(const std::string ip,
                const uint32_t addr_ofs,
                const uint32_t reg)
```
A function to write a 1-byte register to the SiTCP reserved area. The address starts from 0xffffff00, and the offset from there is specified by `addr_ofs`. The EEPROM area cannot be accessed by this function.

```C++
void ReadSiTCP(const std::string ip,
                   const uint32_t addr_ofs)
```
A function to read a 1-byte register from the SiTCP reserved area. `addr_ofs` has the same meaning with WriteSiTCP.

```C++
void EraseEEPROM(const std::string ip)
```
A special function for erase all EEPROMs. The SiTCP license file will need to be rewritten, because IP and MAC addresses and license information will be erased. This function is used to solve the problem reported in the SiTCP community of BBT, titled: "Once a TCP connection is established, it cannot be reconnected for a while". See the relevant thread on the BBT web page for details.

**Common**<br>
The controlling finctions for FlashMemoryProgrammer (FMP) and Self Diagnosis System (SDS) are collected. Since these are common parts of HUL's FW, the registers and register address are stored in  `RegisterMapCommon.hh`.
In the following, behavior of major functions are explained, as they provide  examples of control on HUL.

assert_bctreset
  : Asserts soft reset signal  `BCT::Reset` in the FW.

erase_eeprom
  : An executable that calls the EraseEEPROM of SiTCPController.

reconfig_fpga
  : An executable that actively reconfigures the FPGA from SPI flash memory, as usually happens when the power is turned on. This program is used when the FPGA received a radiation damage, or a new MCS is downloaded to the SPI flash memory using FMP, etc.

read_xadc
  : Access the XADC interface in the SDS to get the FPGA temperature, VCCINT voltage, and VCCAUX voltage.

read_sem
  : Access the SEM interface in the SDS to read the number of single event upset (SEU) corrections, the SEM's status (watchdog), and whether uncorrectable radiation damage has been detected (uncorrectable). The number of SEU correction returns to 0 by  issuing reset signals higher than or equal to the BCT reset or accessing for write to `SemRstCorCount`. If "Uncorrectable" flag is set to 1, the FPGA needs to be reconfigured.

reset_sem
  : Assert a soft reset to SEM.

inject_sem_error
  : Use SEM to artificially generate a SEU for debugging. Since DRP is used to access SEM, more could be done with SEM. See Xilinx PG036 for more information.

flash_memory_programmer
  : A program to writie a MCS file to SPI flash memory via SiTCP. It converts the MCS (text file) to binary, check the SPI memory model number, memory erase, program, readback and verify in order.

mcs_converter
  : A program to convert a MCS file to a binary in advance.

check_spi_device
  : Check the model number of the SPI flash memory on HUL. Use it when you don't know what's on board

verify_mcs
  : Read back from SPI flash memory and check for consistency with the specified MCS file.
## Programs for each FW

Most FWs only implement the `debug` and` daq` executables. `daq` is an executable to acquire data. It is a sample code of data acquision (DAQ). `RegisterMap.hh` contains the FW-specific register address and registers. This section describes FW-specific programs.

DaqFuncs.cc
  : This is the collection of source codes for DAQ. It is probably  better to use the ConnectSocket, Event_Cycle, and receive functions as they are. Also, SetTdcWindow in MH-TDC should be as it is. Basically, the flow one RUN is, configuring the measurement block, open the gate with DCT, keep calling EventCycle, and close the gate with DCT when acumulation is over.
  : Since the buffer in HUL is flushed by calling EventCycle until it times out after closing the Gate, be sure to perform
```while (-1! = EvenCycle)
```
processing. Otherwise, events remained in the previous RUN may be returned at the beginning of the next RUN.

daq
  : Read data from the HUL. Give the IP address, RUN number, and number of events as argumentsa as follows `./bin/daq [ip address] [RUN no] [# of events]`. The output file will be created as `data/run1.dat` in the `data` sub-directory of where the command was issued. So, the programs is expected to be run under each firmware directory, where the `data` sub-directory has been created by `make` command.
  : During data aquisition, read data are displayed once in some events for debuging.

### HR-TDC

initialize
  : Initialize the DDR receiver and calibrate the LUT for the estimator using the calibration clock. Must be run once after the power is up.

decoder
  : A decoder provided for debugging. Give the RUN number as follows `./bin/decoder [RUN no]`, the program reads a data from the `data` directory and reate a rootfile into the `rootfile` directory. The programs is expected to be run under each firmware directory.

**The structure of TTree Branches**

<table class="vmgr-table">
  <thead><tr>
    <th class="nowrap"><span class="mgr-20">Branch name</span></th>
    <th>Description</th>
  </tr></thead>
  <tbody>
  <tr>
    <td>through</td>
    <td>Indicates register setting for `TDC::Through` in mezzanine HR-TDC. Index 0 and 1 correspond to slot U and slot D, respectively. </td>
  </tr>
  <tr>
    <td>stop_out</td>
    <td>Indicates register setting for `TDC::StopDout` in mezzanine HR-TDC. Index 0 and 1 correspond to slot U and slot D, respectively. </td>
  </tr>
  <tr>
    <td>over_flow</td>
    <td>Indicates over flow status stored in sub-headers. Index 0 and 1 correspond to slot U and slot D, respectively. </td>
  </tr>
  <tr><td class="tcenter" colspan=2><b>Decoded common stop TDC data.</b></td></tr>
  <tr>
    <td>stop_fine_count</td>
    <td>Fine count (lower 13-bit of TDC data) for common stop data. It's avairable when TDC::StopDout is true.</td>
  </tr>
  <tr>
    <td>stop_estimator</td>
    <td>Estimator value for common stop data. It's avairable when TDC::StopDout is true.</td>
  </tr>
  <tr>
    <td>stop_coarse_count</td>
    <td>Coarse count (upper 11-bit of TDC data) for common stop data. It's avairable when TDC::StopDout is true.</td>
  </tr>
  <tr>
    <td>common_stop</td>
    <td>TDC value for common stop. It's avairable when TDC::StopDout is true.</td>
  </tr>
  <tr><td class="tcenter" colspan=2><b>Decoded data for leading edge measurement.</b></td></tr>
  <tr>
    <td>num_hit</td>
    <td>The number of data for leading the edge measurement.</td>
  </tr>
  <tr>
    <td>num_hit_ch</td>
    <td>The number of data in each channel for leading the edge measurement.</td>
  </tr>
  <tr>
    <td>channel</td>
    <td>Channel number for the decoded data word. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr>
    <td>fine_count</td>
    <td>Fine count (lower 13-bit of TDC data) for the TDC data word. It's avairable when TDC::Through is true. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr>
    <td>estimator</td>
    <td>Estimator value for each TDC data word. It's avairable when TDC::Through is false. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr>
    <td>coarse_count</td>
    <td>Coarse count (upper 11-bit of TDC data) value for each TDC data word. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr>
    <td>tdc_leading</td>
    <td>TDC value for each TDC data word. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr><td class="tcenter" colspan=2><b>Decoded data for trailing edge measurement.</b></td></tr>
  <tr>
    <td>tdc_trailing</td>
    <td>TDC value for each TDC data word. The index number indicates the order in which they were decoded.</td>
  </tr>
  <tr><td class="tcenter" colspan=5>Branches starting with 't' are decoded data values for the trailing edge measurement. </td></tr>
  </body>
</table>

xadc and sem
  : Since SDS is also implemented in the Mezzanine HR-TDC, an executable is provided to access Mezzanine's XADC and SEM.


### Three-dimensional matrix trigger

The source code for this FW is in the folder Mtx3D_E03E42, and the sample registers are placed under `Mtx3D_E03E42/example_register`. Each executable is used to read the register file and reflect it to the FPGA. In the register file, lines starting with # are treated as comments.

**Register files**

dwg/dwg_register.txt
  : Sets the DWG register in each block. The amount and width of delay for each signal can be adjusted. The range of values that can be taken is as written in the text file.

mtx/matrix3d(2d)_pattern.txt
  : Set up the matrix pattern for trigger output. 3-dimensional patterns are expanded into multi-bit shift registers as explained in the FW chapter. 8-bit patterns are set up in order, starting from the younger combination of TOF and SCH. Each line contains the segment number of the SCH, but the software does not read the SCH number. This order is determined by the shift register arrangement inside the FPGA, so the lines cannot be replaced in the text file. In the case of matrix2d_pattern, each 2D matrix element is arranged in 1D.

nimo/nimo_register.txt
  : This register determines which internal signal is output to the NIM output port.

**Executable files**

load_register
  : Reads the dwg register file and sets it to the FPGA.

load_matrix3d(2d)
  : Read the matrix_pattern register file and set it to FPGA.

set_nimo
  : Read the nimo register file and set it to FPGA.

### Mass trigger

The source code for this FW is in a folder called MsT. Under `MsT/example_register` is a collection of sample registers. In the register file, lines starting with # are treated as comments, and because MsT has both DAQ function and trigger generation function, there are both executables for data collection and register setting.


