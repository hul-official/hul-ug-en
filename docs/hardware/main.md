# Hardware

## HUL controller module

Hadron Universal Logic (HUL) controller module is VME 6U size printed circuit board, and is called simply as HUL hereafter.  The catalog number in GND Ltd. is: **GND catalog number (ジーエヌディー管理番号) GN-1573-1**

### Detailed Specifications

* Input ports
    * 64 pairs of differential inputs (KEL 8831E connector)
    * Supports LVDS, ECL, PECL, LVPECL etc.
    * 4 ports of NIM inputs
* Output ports
    * 4 ports of NIM outputs
* Mezzanine slots
    * 2 slots
    * Directly connected to FPGA via duplex LVDS 32 pairs
    * Power suppy to Mezzanine board: +3.3 V from HUL
* Communication interface
    * RJ45: GbE (1000BASE-T)
    * VME J1 is not supported
* FPGA
    * Xilinx Kintex7 (XC7K160T-1FBG676C)
* Configuration memory chips
    * SPI flash in 3.3V (one of the followings depending on the manufacture date)
        * N25Q128A13EF840E
        * MT25QL512ABB1EW9-0SIT
        * S25FL256SAGNFI001
    * SPI (synchronous serial interface) in serial configuration mode
* Clock source
    * 50 MHz LVCMOS (~50 ppm)
        * Peak-To-Peak jitter 30ps
* Trigger bus
    * KEK-VME J0
    * HUL may be configured as a bus driver or a receiver
* Power supply
    * DC +5V
    * supplied from a AC/DC adapter, or the VME-J1 connector
* Power consumption
    * static: 0.5~0.7 W @3.3 V (mainly at ethernet PHY) and 0.5~0.7 W @1.0 V (mainly at FPGA)
    * dynamic: strongly depends on the FPGA firmware

A picture of HUL and block diagram are shown below

![hul-photo](hul-photo.png "Photo and block diagram of HUL controller."){: style="display:block;  margin:auto" width=100% }
<!-- ![hul-block](hul-block.png "HUL controllerのブロック図"){: style="display:block;  margin:auto" width=75% } -->

Main port U (D)
: Fixed input ports in the front. The connector is the half-pitch 68 poles (KEL 8831E-068-170L-F). A cable with the compatible connector is supposed to be manufactured. Channel assignment is 0-31 for U and 32-63 for D, with lower number in the marker side. The signal grounds are A1A2 and B1B2 pairs, right below the marker. The inputs support differential signals in LVDS, ECL, PECL and LVPECL levels with the common mode voltage in the  -4 V ~ +5 V range. When feeding an emitter-follower type signal levels, such as ECL, the signal current must be controlled in the driver side. There is no register for the current control in the HUL side.
: The fixed input ports converts the differential signals to LVCMOS before feeding to FPGA. This limits the maximum repetation speed to 560 Mbps. If the signal rate is higher than that, consider using a mezzanine card. For an application to wire chambers, where Amp Shaper Discriminator (ASD) outputs are fed into HUL, the repetition rate limit is not a problem.

NIM IN
: 4 intputs of NIM level. Channel assignments are written on the board. **Channel number starts with 1**

NIM OUT
: 4 outputs in NIM level. Channel assignments are written on the board. **Channel number starts with 1**

Ethernet connection (LAN)
: RJ45 type ethernet connector. Gigabit Ethernet PHY chip is connected. Used for PC-FPGA communication via SiTCP.

Mezzanine slot U(D)
: Slots to install mezannine cards. HUL has two of 64 pole connectors MOLEX-071439-0464 for one card. The upper connector has the signals and the lower has the power supply and the ground. Mezzanine cards may employ a few models of connectors with different heights, but if MOLEX-071436-1464 is employed, the card will be supported by three of 9 mm stand + 0.5 mm washer sets.
: The signals are 32 pairs of differential lines to FPGA. There is no component to determine the signal direction between mezzanine connectors to FPGA, so that duplex LVDS may be adopted for the signal levels. The design of the mezzanine card and the FPGA firmware determine the actual signal levels and directions. If inputs are assigned to Slot U and D, the channel numbers will be 64-95 and 96-127 respectively, with the offset (0-31 and 32-63) from the main ports of the board. Some of the polarities p/n are reversed between the mezzanine connectors to FPGA owing to the simplicity of the pattern layout. The VHDL source `MZN_NetAssign.vhd` takes care of the polarities of the differential signals by inserting logical inverters (NOTs) to the signals.
: Mezzanine slots supply +3.3 V to the card. The current allowance will be 4-5 Amps (13-16 W) to the card, with the 6 Amps supplied from the power and 1-2 Amps consumed by HUL itself.

CN14
: Connector for JTAG protocol. PC may download the FPGA firmware via USB-JTAG downloader by Xilinx. How to download bitstream or MCS are described more in detail in Chapter 7.

SW1
: The switch which defines whether HUL is a VME J0 bus driver or a receiver. SW1-8 must be all OFF to be a receiver (default). SW1-8 must be all ON to be a driver. Do not define more than one driver in a VME crate, as such situation will short circuit the bus line and causes a damage.

SW2
: User defined dip switch. The role is defined in FPGA firmware.

SW3
: User defined push switch. It generates a pulse to send to FPGA. In the exisitng firmware, it causes the highest reset action. The pulse logic is negative, changing from 1 to 0 if pressed.

JP1
: Reserved. Keep it open.

JP2, 3
: Close to take power from VME J1. DC +5V as the supply.

JP4, 5
: Close to take power from a AC/DC adapter.

AC/DC
: Standard socket for a DC power input (OD 5.5 mm, ID 2.1 mm, center plus).

FUSE
: 5 Amps fuse. Compatible to a standard fuse, such as PICO® fuse by Littelfuse.

VME J1
: Connector to a VME crate. Only the power supply (+5V) is connected.

VME J0
: Connector to KEK-VME J0 bus. There is no connection to the power supply on J0 bus. **Equipped as a default, but is an optional.** Order the board without CN9 to remove this connector.

## HUL Mezzanine cards

### HUL Mezzanine Drift Chamber Receiver (DCR) Ver.1

Drift Chamber Receiver (DCR) Ver.1 is the input repeater circuit board for the Amp Shaper Discriminator (ASD) card designed for drift chambers (GND catalog number: GNA200). It repeats 32 channels of signal from GNA200 into LVDS levels, so that HUL can read. Ver.1 keeps the differential characteristics to FPGA with a higher timing resolution, it is superior to the Ver.2 or the HUL main input ports. However, these advantages are subtle, and Ver.2 is upper conpatible with more kinds of supported signal levels.
**GND catalog number (ジーエヌディー管理番号): GN-1573-S1**

![DCRV1](mzndcr-v1.png "mezzanine DCR v1"){: style="display:block;  margin:auto" width=50% }

Input Port
: LVDS input ports. The connector is a half-pitch 68 pole (KEL 8831E-068-170L-F). A compatible connector must be used with cables. The marker side is lower in the channel number, as in the HUL on-board inputs. The grounds are A1A2 and B1B2 pair, directory below the marker. Due to the simplicity of the board pattern, DCR ver.1 and ver.2 both have a swapping of the channel numbers and polarity p/n. The VHDL source `DCR_NetAssign.vhd` takes care of the channel / polarity order to be correct, as shown in the [figure below](#DCRNET).

![DCRNET](dcrnet.png "DCR has net swaps and corrections"){: #DCRNET width=50% }

LVDS repeater
: Receives LVDS signals and repeats to FPGA. These ICs protect FPGA from unintentinal signal disturbances, such as discharges.

### HUL Mezzanine Drift Chamber Receiver (DCR) Ver.2

Drift Chamber Receiver (DCR) Ver.2 has the same role with ver.1, but employs the same differential receiver ICs with those used in HUL on-board main input ports. This enables acceptance of various levels of differential signals (LVDS, ECL, PECL, LVPECL etc) as for the HUL on-board inputs. For the purpose of input ports extention, ver.2 is recommended.
**GND catalog number (ジーエヌディー管理番号): GN-1626-1**

![DCRV2](mzndcr-v2.png "mezzanine DCR v2"){: width=50% }

Input Port
: Mechanically the same with DCR Ver.1. Supports differential signals with the common mode voltages ranging from -4 V to +5 V. The differential signal is converted to LVCMOS (as in the on-board input ports) and subsequently converted to LVDS to feed to FPGA. The VHDL code DCR_NetAssign.vhd takes care of the channel and polarity p/n swaps as in ver. 1.

### HUL Receiver Module (HRM)

HUL Receiver Module (HRM) receives the trigger signals and event numbers distributed from Master Triger Module (MTM: GNN-570)  employed in J-PARC Hadron Hall experiments, and returns BUSY signals back to MTM.  HRM de-serializes the trigger signals and event numbers transmitted through the Category 5e twist-pair cables, and converts to a bus signal format to FPGA. In return, HRM serializes the BUSY and RSV2 signals from FPGA to be transmitted to MTM. The trigger signals to and BUSY signals from HRM are all processed in the FPGA on HUL, enabling the HUL to function as a J0 bus controller of KEK-VME crates.
**GND catalog number (ジーエヌディー管理番号): GN-1627-1**

![HRM](mznhrm.png "HUL receiver module (HRM)"){: width=50% }

Port A
: Trigger inpurt port A. Should be connected with a twist pair cable to MTM port A or repeater port A.

Port B
: Trigger input port B. Should be connected with a twist pair cable to MTM port B or repeater port B.

LED (LOCK)
: Lights green if the event tag bit from Port B is correctly decoded, and the PLL lock of the clock is high.

LED (BUSY)
: Lights red if the BUSY to MTM is high. However, HRM reflects only the BUSY status from FPGA, which should be independently checked if it correctly reflect the the create bus BUSY signals.

JP1
: Controls RRFB, meaning whether the de-serialized event tag outputs synchlonize to RCLK clock rising edge (R) or falling edge (F). Default is R. This is a 3-pins pin header. Short the R side.


**Inputs/Outputs to/from Mezzanine HRM**

| Signal | Direction | Description |
| ------ | --------- | -------- |
| RCLK   | IN        | Clock decoded from the event tag from port B|
| LEVEL1 | IN        | Level1 trigger signal |
| LEVEL2 | IN        | Level2 trigger signal |
| Clear  | IN        | Fast clear signal |
| RSV1   | IN        | Reserve 1 signal from MTM|
| LOCK   | IN        | Lock bit of PLL, showing that the clock is correctly decoded. |
| SNINC  | IN        | Spill Number Increment. FPGA may count the spill numbers.|
| Event counter | IN | 12 bit event number from MTM |
| Spill counter | IN | 8 bit spill number from MTM |
| RRFB   | OUT       | Timing for decoded tag information. High for rising edge (R) and Low for falling edge (F) of RCLK. |
| BUSY   | OUT       | BUSY signal to MTM |
| RSV2   | OUT       | Reserve 2 signal to MTM|


### HUL Mezzanine differential signal transmitter LVDS (DTL)

Mezzanine DTL is a LVDS output buffer from HUL-FPGA to other devices. The circuit is the same with DCRv1, except the input/output directions of the LVDS repeaters. The channel assignment is the same with DCRv1. HUL-FPGA must output LVDS signal to drive this card.
**GND catalog number (ジーエヌディー管理番号): GN-1724-1**

![DTL](mzndtl.png "Mezzanine DTL"){: width=50% }

Output Port
: LVDS output ports. The mechanical specification, signal and ground assignments are the same with DCR v1. This mezzanine card requires to use the VHDL source `DCR_NetAssign.vhd`, but the input/output direction is opposite from the case in DCRv1.

### HUL Mezzanine NIM extension (NIM-Ex)

Mezzanine NIM-Ex extends the NIM inputs/outputs of HUL. It has 12 LEMO connectors grouped in 6 pairs which are independently selected for inputs or outputs by switch settings. The same IC is employed as used in HUL NIM IO ports, and the speed is the same.

<span class="myred"> NIM-Ex card consumes 5.5W, being rather large. </span> The card generates -3.3 V on-board and may become hot. Air cooling fan is strongly recommended. Otherwise, the mezzanine card may break by the self-heating.
<span class="myred"> There are numerous mistakes on the circuit diagrams. </span> An example design is included in the skeleton project of mezzanine board, because the errorous circuit net prevents a guess of correct XDC and HDL code.

![NIMEX](mznnimex.png "Mezzanine NIM-Ex"){: width=50% }

LEMO
: 12 channel LEMO connectors. Example sckeleton design includes `NimEx_NetAssign.vhd`, which has the net number associated to the physical channels as [figure above](#NIMEX).

Dir SW
: Signal direction in groups of 2 channels. Defined for OUTPUTs if the switch is toward the LEMO connector; defined INPUTS if otherwise.

### HUL Mezzanine High-resolution TDC

Mezzanine HR-TDC is capable of measuring time difference in ~30 ps resolution in common stop and multi-hit mode. It has a dedicated FPGA for the time measurement on the mezzanine card, and requires controls and data transfer operations from HUL. The function of the mezzanine is to measure time, to form data in a pre-fixed format and transfer the data to FPGA on HUL by the trigger input. Depending on the firmware of FPGA on HUL, this mezzanine is able to act as a simple TDC or play a more complicated role such as Time of Flight (TOF) trigger. Detailed action and control is described in Chapter 4.

The FPGA on this mezzanine card is Kintex-7 160T-1, the same chip as used on HUL. One mezzanine takes 32 channels of timing signals, capable to measure both the leading and trailing edges. The sinal input level is exclusively LVDS, not supporting ECL. The clock for FPGA is selectable either from the quartz on the mezzanine or from HUL. The power is +3.3 V taken from HUL, and other voltages are generated on the mezzanine card via low drop-out (LDO) regulators. The power consumption is 5 W / card, which is rather high. If the +3.3 V power supply chip on HUL is  LMZ30604RKGT (max. 4 Amps), it is insufficient to drive two mezzanine cards. In such cases, make sure the chip on HUL is **LMZ30606RKGT (max. 6 Amps)**. The power consuption of the entire board will become almost 20 W when two HR-TDC mezzanine boards are installed. Make sure that the board is well cooled by a fan. If the cooling of the FPGAs is not sufficient, they became unstable with progressively increased leak currents. It is strongly recommended that a VME crate with a fan is used. (Also, stable ground is necessary to reach to the high timing resolution.)
**GND catalog number (ジーエヌディー管理番号)：GN-1644-1**

![MZNHRTDC](mznhrtdc.png "Mezzanine HR-TDC"){: width=50% }

Input Port
: Signal inputs which supports only LVDS levels. Connector is a half-pitch 68 pole (KEL 8831E-068-170L-F). The ground is assigned on A1A2 and B1B2, right below the marker. Channel assignment is 0-31 from the marker. There is no swap of the channels.

CN4
: JTAG connector for firmware download. FPGA and SPI flash memory may be accessed. The SPI flash memory chp is N25Q128A11EF840E or MT25QU256ABA1EW9-0SIT with 1.8V power inputs. Please configure / identify the chip correctly when downloading MCS onto the SPI chip.

SW1
: Dip switch to control the FPGA of the mezzanine card. The current firmware selects the clock source from the quartz on the mezzanine card or from the HUL.

X1
: 100 MHz quartz oscillator. The frequency must be the same if HUL clock is used.

**SPI flash memory chip version**<br>
Mezzanine HR-TDC has two versions for the flash memory chip, depending on the date of the manufacture. To correctly identify the flash memory chip, Vivado (the FPGA tool on PC) is required. It is safer to mark the SPI flash memory chip version with a note on the board.

<ul>
  <li> N25Q128A11EF840E (pre 2020 manufacture)
  <li> MT25QU256ABA1EW9-0SIT (since 2020)
</ul>
