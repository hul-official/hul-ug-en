# Introduction

## Update history

**2017.12.19**

* In Chapter 2, added a section about Mezzanine DTL.
*	In Chapter 2, added a section about Mezzanine HR-TDC.
*	Consistent behavior of the reset sequence in the existing firmware (described in Chapter 4).
*	Explicitly stated that 100 Mbps ethernet communication is not supported in the exisiting firmware.
*	Added a bit in the data header of the existing firmware showing that HRM exists (described in Chapter 4).
*	Stability improvement of HUL MH-TDC in high rates. Increased the event word size to 12 bits. Confirmed no event slips up to $`10^9`$ events.
*	Added HR-TDC section in Chapter 4.
*	Added a few practical use in Chapter 7.
*	Description about the board with SPI flash chip changed to MT25QL512.
*	Description of simple tests.
*	Description of how to use multiple HUL in a VME crate.
*	Usage of HR-TDC.
*	Modification to sitcp_controller.cc in controlling C++ software package. Sleep configulation in Reset_SiTCP.
*	Removed gzfilter.hh from the controlling C++ software package, because it is strongly dependent to the gcc version. Removed data compression option.

**2018.2.2**

*	Resolved an issue of returning a wrong event ID by +1 when event-tag is received from J0 bus by HRM, Scaler, MH-TDC and  HRTDC_BASE, relative to the module with HRM installed.
*   Resolved an issue of data returning out of the search window range from Mezzanine HR-TDC and MH-TDC.
*	Resolved an issue of Local bus hang in case of BCT::Reset in HR-TDC_BASE.
*	Described in Chapter 7 the issue that no triger could be received in the J0 slave modules after a module reset in case of Level2 trigger in J0 on a VME crate (not resolved).
*	In Chapter 7, described the case where an event slip has occured.

**2018.8.22**

*	In Chapter 7, described a board which has SPI flash upgraded to S25FL256SAGNFI001.
*	In Chapter 7, described the handling necessary in the original SPI flash memory, N25Q128A

**2021.11.10**

*	Firmware is major updated to version 3.
*	Major updates of the descriptions in the User Guide

**2022.01.13**

*	Release the English version user guide.

**2022.03.18**

*	Correct the description of the mezzanine HR-TDC sub-header structure.

**2023.01.17**

* Released Mezzanine HR-TDC v5.0 and HUL HRTDC BASE v4.0. Updated description of what changed.
* Removed MifFunc.cc from software. Introduced BctBusBridge.cc instead.
* Fixed a bug that the self-busy length of HUL HRTDC BASE is short and there is a valley in the busy signal.

**2023.02.24**

* Released HUL HRTDC BASE v4.1. The bug-fixed version of v4.0, which does not work correctly.

**2023.08.19**

* Fixed incorrect description of Mezzanine HR-TDC clock frequency.

## Module overview

Hadron Universal Logic (HUL) module has been developed as an upgrade from Tohoku Universal Logic (TUL) which was used in Hadron Hall experiments, J-PARC. The controlling field programmable gate array (FPGA) is upgraded to Xilinx Kintex 7, enabling more complicated logic conditions run in a higher speed. HUL has two fixed input connectors and two mezzanine slots the latter of which enable extensions to various experimental needs by installing mezzanine cards. These two connectors and slots are connected to FPGA by 64 (32x2) fixed input lines and 64 (32x2) pair differential lines, respectively, enabling handling of 128 channel maximum inputs. The 64 pair differential lines are directly connected to the mezzanine base connectors, so that input/output is programmable to the mezzanine cards.
HUL is equipped with the data communication ethernet interface (GbE) and the trigger input/output busline (KEK-VME J0) which TUL did not have. Communication to the data-taking PC employs UDP and TCP protocols supplied by the SiTCP technology, which is a FPGA-based hardware implimentation of TCP/IP, supporting 1Gbps TCP communication via an ethernet cable (Cat.5 and above). UDP communication is extended by remote-bus control protocol (RBCP) of SiTCP, which supports the addressed access to the memory region of HUL in slow-controls. KEK-VME J0 bus is 8 pairs of triggering bus in M-LVDS signal levels. It consists of 7 pairs of transmission lines and one pair of output (BUSY). HUL may act as the bus controller or the slave receiver of the J0 bus.
HUL is developped in an Open-It project "Hadron Universal Logic Module", employing the technological developments achieved in the Open-It consourtium. The interectual property and its usage is subject to the terms of the Open-It consourtium.
[Open-It](http://openit.kek.jp/project/HUL/HUL)


